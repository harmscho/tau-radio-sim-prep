/* This is a new version of the Vector struct in Toymodel.h */

#ifndef VECTOR3D_H
#define VECTOR3D_H


#include <math.h>
#include <iostream>

using namespace std;

class Vector3D{
    
public:

    double x,y,z;          //Cartesian coordinates
    double r,theta,phi;    //Spherical coordinates (angles in rad)
    
    
    //Constructors
    ////////////////////////
    
    //Default constructor
    Vector3D();
	
    //Set either cartesian or spherical coordinates (IsCartesian bool control)
    Vector3D(double c1, double c2, double c3, bool IsCartesian);
    

    //Set Methods ///////////////////
    /////////////////////////////////////////////////////////

    //Set vector using cartesian coordinates 
    //Also automatically sets spherical coordinates from cartesian input
    void SetCart(double xin,double yin,double zin);

    //Set vector using spherical coordinates 
    //Also automatically sets cartesian coordinates from spherical input
    void SetSpher(double rin,double thetain,double phiin);

    //Transforms and operations ///////////////
    ///////////////////////////////////////////////////
    
    //Make the vector a unit vector
    void MakeUnit();

    //Multiplies this vector by a scalar 
    void Multiply(double k);

    //Divides this vector by a scalar
    void Divide(double k);

    //Add another vector to this one
    void Add(Vector3D v);

    //Subtract another vector from this one
    void Subtract(Vector3D v);

    
    void Print();

    Vector3D operator *(double k);
    Vector3D operator /(double k);
    Vector3D operator +(Vector3D v);
    Vector3D operator -(Vector3D v);

    Vector3D& operator *=(double k);
    Vector3D& operator /=(double k);
    Vector3D& operator +=(Vector3D v);
    Vector3D& operator -=(Vector3D v);
};

//NOTE: Replaced this overloaded operators by class member ones
///////////////////////////////////////////////////////////////

//v=v1+v2
//Vector3D operator+(Vector3D v1,Vector3D v2);

//v=v1-v2
//Vector3D operator-(Vector3D v1,Vector3D v2);

//v=k*vin , k is scalar vin is vector
//Vector3D operator *(double k,Vector3D vin);
//////////////////////////////////////////////////////////////


//Dot Product
double DotProd(Vector3D v1,Vector3D v2);

//Cross Product
Vector3D CrossProd(Vector3D v1,Vector3D v2);

//Rotate
Vector3D Rotate(double Theta,double Phi, Vector3D vIn);


//Rotate Back (inverse rotation)
Vector3D RotateBack(double Theta,double Phi, Vector3D vIn);


//returns angle between vec1 and vec2
double VecAngle(Vector3D vec1,Vector3D vec2);




#endif
