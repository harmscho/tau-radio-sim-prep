#include "NuDecay_ROOT.h"
#include "NuSimPreparator.h"
#include "AntennaArray.h"

int main()
{
    //Pars
    string name="NuDecaysE1e18.root",empty;
    int decayn=3;
    double thetadeg=0.0;
    double phideg=0.;
    double groundaltitude=3000.;
    double decayheight=1700.0;
    double desiredtauenergy=1e17;
    double thinningenergy=1.e-4;
    double dxmaxgcm2=500.0;
    double MaxAngleInDeg=0.3;
    double MaxAngleOutDeg=0.3;
    //array
    double d=100.;
    double length=2000.; 
    double width=2000.;
    //double dxmax=65076.;
    double anitaaltitude=36000.;
    //double dxmax=10000; 
    bool invertphi=false;
    //bool invertphi=true;
    


    cout<<endl<<"TEST 3:"<<endl;

    NuSimPreparator event2(desiredtauenergy,thetadeg,phideg,decayheight,groundaltitude,name,decayn,invertphi);
    
    cout<<"InvertPhi="<<event2.GetInvertPhi()<<endl;
    
    

    event2.ReadDecay();
    event2.SetThinningEnergy(thinningenergy);

    event2.SetVerboseOn();
    event2.SetDXmaxgcm2(dxmaxgcm2);
    event2.SetMaxAngleInDeg(MaxAngleInDeg);
    event2.SetMaxAngleOutDeg(MaxAngleOutDeg);

    
        
    //event2.CreatePerpendicularAntennaArray(d, length, width, dxmax);
    event2.CreateAnitaPerpendicularAntennaArray(d, length, width, anitaaltitude);
    event2.Array.WriteArrayCoreCSFile();

    event2.CreateSpecialParticleFile();
    event2.CreateAiresInpFile();
    event2.DumpParticleArray();
    //AntennaArray array;
    cout<<"InvertPhi="<<event2.GetInvertPhi()<<endl;
    
	
}
