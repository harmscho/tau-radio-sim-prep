/* This is a simple library to read Pablo's Nu decay root files in order to create jet information for zhaires runs. It of course uses ROOT, while the general ZHSNutools does not. This is why it's implemented separately

Possibly in the future deacy info could be stored in "generic" binary files, so no ROOT will be needed

Washington Carvalho April 2015*/

#ifndef NUDECAY_ROOT_H
#define NUDECAY_ROOT_H

#include "TFile.h"
#include "TTree.h"
#include <vector>
#include <iostream>
#include <string>
#include <cstdlib>
#include "Particles.h"
#define VERSIONDR "0.1" 
using namespace std;


struct NuDecay_ROOT
{
    //CONSTRUCTORS   ///////////////////////////////////////////

    //default constructor
    NuDecay_ROOT();
    //Constructor that sets input ROOT file
    NuDecay_ROOT(string filename);
    //Constructor that sets input file and decay number
    NuDecay_ROOT(string filename, int decayn);
    ///////////////////////////////////////////////////////////
    
    //MEMBER VARIABLES  ///////////////////////////////////////

    int nPart;               //Number of secondary particles 
    float TotalE; 
    float ShowerTotalE;
    float DecayXc;   //Not used anymore
    float DecayH10;  //Not used anymore
    float DecayH5;   //Not used anymore
    float Theta;     //Not used anymore
    float Phi;       //Not used anymore

    
    //Particle vector containing all particle data of the given decay
    vector<Particles> pArray; 
    
    

    int DecayN;       //Number of decay from input file to use
    string InputFile;
    
    bool InputIsSet, DecayNIsSet,pArrayIsSet,Verbose;
    
    ////////////////////////////////////////////////////////
    
    //MEMBER METHODS  //////////////////////////////////////

    void SetInputFile(string filename);  //Sets input file (.root)

    void SetDecayN(int decayn);          //Sets decay to use

    void ReadDecay();                    //Read particle info from decay
    
    void ReadDecay(string filename, int decayn);  

    void SetVerboseOn() {Verbose=true;};

    void SetVerboseOff() {Verbose=false;};

    vector<Particles> GetpArray(){return pArray;};


    double GetTotalE(){return TotalE;};
    double GetShowerTotalE(){return ShowerTotalE;};

    void Hello();
    
    //double GetDecayXc(){return DecayXc;}; //Not used anymore

    ////////////////////////////////////////////////////////
    
    

};

int GetNDecays(string filename)
{
    //open file (untested!)
    TFile *fin=TFile::Open(filename.c_str(),"READ");

    //read root tree
    TTree *mainTree=(TTree*)fin->Get("mainTree;1");
    
    return mainTree->GetEntries(); //Number of decays in the tree
}    



#endif
