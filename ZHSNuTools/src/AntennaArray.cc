/* This is a simple library to read Pablo's Nu decay root files in order to create jet information for zhaires runs. It of course uses ROOT, while the general NuSimPreparator does not. This is why it's implemented separately

Possibly in the future deacy info could be stored in "generic" binary files, so no ROOT will be needed

This is the struct/class that deals with creating and adjusting an array for the simulation. See Array.h for more info.

Washington Carvalho April 2015*/

#include "AntennaArray.h"

////////////////////////////////////////////////////////////
//CONSTRUCTORS   ///////////////////////////////////////////
////////////////////////////////////////////////////////////

//default constructor
AntennaArray::AntennaArray():
D(-1),
Length(-1),
Width(-1),
ArrayDimensionsAreSet(false),
CoreIsSet(false),
XmaxIsSet(false),
ArrayIsSet(false),
ArrayCoreCSIsSet(false),
Verbose(true),
ArrayCoreCSFileName(""),
ArrayFileName(""),
ShowerIsUpgoingInCoreCS(true),
ArrayCoreCSWasTransformed(false),
ArrayFilteredIsSet(false),
MinZ(-1.e10),
MaxZ(1.e10),
MinPsi(-1.e10),
MaxPsi(1.e10)
{

    Hello();

}

//Constructor that sets distance D (in m) between antennas,
    //the  Length (in m) and the Width (in m) of the array.
AntennaArray::AntennaArray(double d,double length, double width):
D(-1),
Length(-1),
Width(-1),
ArrayDimensionsAreSet(false),
CoreIsSet(false),
XmaxIsSet(false),
ArrayIsSet(false),
ArrayCoreCSIsSet(false),
Verbose(true),
ArrayCoreCSFileName(""),
ArrayFileName(""),
ShowerIsUpgoingInCoreCS(true),
ArrayCoreCSWasTransformed(false),
ArrayFilteredIsSet(false),
MinZ(-1.e10),
MaxZ(1.e10),
MinPsi(-1.e10),
MaxPsi(1.e10)
{
    Hello();
    //test if dimensions are not negative
    if( !CheckDimensions(d,length,width) || aArray.size()>0 )
    {
	cout<<endl<<"(EE) AntennaArray constructor error:"<<endl;
	cout<<"     Array dimensions D, Length and/or Width out of bounds! Exiting..."<<endl;
	if(aArray.size()>0) cout<<"     aArray not empty!!! Why??"<<endl;
	exit(-1);
    }

    SetDimensions(d,length,width);
    
}





AntennaArray::AntennaArray(double d,double length, double width, double corex, double corey, double corez):
D(-1),
Length(-1),
Width(-1),
ArrayDimensionsAreSet(false),
CoreIsSet(false),
XmaxIsSet(false),
ArrayIsSet(false),
ArrayCoreCSIsSet(false),
Verbose(true),
ArrayCoreCSFileName(""),
ArrayFileName(""),
ShowerIsUpgoingInCoreCS(true),
ArrayCoreCSWasTransformed(false),
ArrayFilteredIsSet(false),
MinZ(-1.e10),
MaxZ(1.e10),
MinPsi(-1.e10),
MaxPsi(1.e10)
{
    Hello();
    //test if dimensions are not negative
    if( !CheckDimensions(d,length,width) || aArray.size()>0 )
    {
	cout<<endl<<"(EE) AntennaArray constructor error:"<<endl;
	cout<<"     Array dimensions D, Length and/or Width out of bounds! Exiting..."<<endl;
	exit(-1);
    }

    //This check should never fail...   Just a test...
    if(aArray.size()>0) cout<<" (EE)  aArray not empty!!! Why??"<<endl;


    SetDimensions(d,length,width);
    
    SetCore(corex,corey,corez);

}




//Check methods //////////////////////////////////////
////////////////////////////////////////////////////

bool AntennaArray::CheckDimensions(double d, double length, double width)
{
    if(d<=0 || length<=0 || width<=0) return false;
    else return true;
}





//Set methods ////////////////////////////////////////////
////////////////////////////////////////////////////////
void AntennaArray::SetDimensions(double d, double length, double width)
{
    //test if dimensions are not negative
    if( !CheckDimensions(d,length,width) )
    {
	cout<<endl<<"(EE) AntennaArray::SetDimensions error:"<<endl;
	cout<<"     Array dimensions D, Length and/or Width out of bounds! Exiting..."<<endl;
	
	exit(-1);
    }
    
    if(aArray.size()>0)
    {
	cout<<endl<<"(WW) AntennaArray::SetDimensions warning:"<<endl;
	cout<<"     aArray not empty. Reseting it..."<<endl;
	aArray.clear();
	ArrayDimensionsAreSet=false;
	ArrayIsSet=false;
	ArrayCoreCSIsSet=false;
    }
    
    D=d;
    Length=length;
    Width=width;
    ArrayDimensionsAreSet=true;

    //calculate array
    int nSemiLength=Length/2./D; 
    int nSemiWidth=Width/2./D;
    
    int CurrentId=1;

    for(int l=-nSemiLength;l<=nSemiLength;l++)
    {
	for(int w=-nSemiWidth;w<=nSemiWidth;w++)
	{
	    ArrayAntenna ant;
	    ant.x = l * D;
	    ant.y = w * d;
	    ant.z = 0;
	    ant.Id = CurrentId;
	    aArray.push_back(ant);

	    if(Verbose) cout<<"Id: "<<ant.Id<<"\t  x="<<ant.x<<"\t  y="<<ant.y<<"\t  z="<<ant.z<<endl;

	    CurrentId++;
	}


    }

    //Set default Array file name (output)
    std::ostringstream s;
    s<<"Array-D"<<d<<"m-L"<<Length<<"m-W"<<Width<<"m-ArrayCS.dat";
    ArrayFileName=s.str();
    ArrayIsSet=true;  //aArray is calculated!


    if(Verbose) cout<<"Array is set! Default output file name is "<<ArrayFileName<<endl;


}



void AntennaArray::SetCore(double xc, double yc, double zc)
{
    if(ArrayCoreCSIsSet)
    {
    	cout<<endl<<"(WW) AntennaArray::SetCore warning:"<<endl;
    	cout<<"     Array in core coordinates was already set! Will reset it!"<<endl;
    	aArrayCoreCS.clear();
	ArrayCoreCSIsSet=false;

    }

    Core.SetCart(xc,yc,zc);  //Sets Core with cartesian coordinates
    CoreIsSet=true;

    if(!ArrayIsSet)
    {
	cout<<endl<<"Array is not set yet! Will not calculate Array in Core CS."<<endl;
	return;
    }
	
    
    else
    {
	
	cout<<endl<<"calculating Array in Core CS...."<<endl;
	CalculateArrayCoreCS();
	cout<<"Done!"<<endl;

    }

    
}


//Set coords of Xmax in the Core system (in m)
void AntennaArray::SetXmax(double xmaxx,double xmaxy,double xmaxz)
{
    if(!CoreIsSet)
    {
	cout<<endl<<"(EE) AntennaArray::SetXmax error:"<<endl;
	cout<<"     Core is not set yet! You must first set the core position! There is no \"CoreCS\" yet! Exiting..."<<endl;
	exit(-1);
    }
    
    Xmax.SetCart(xmaxx,xmaxy,xmaxz);
    XmaxIsSet=true;

    if(ArrayCoreCSIsSet) CalculatePsi();
}







//"Action" methods
/////////////////////////////////////////

void AntennaArray::CalculateArrayCoreCS()
{
    if(!ArrayIsSet)
    {
	cout<<endl<<"(EE) AntennaArray::CalculateArrayCoreCS error:"<<endl;
	cout<<"     Array is not set yet! You must first set the array geometry! Exiting..."<<endl;
	exit(-1);
    }
    if(!CoreIsSet)
    {
	cout<<endl<<"(EE) AntennaArray::CalculateArrayCoreCS error:"<<endl;
	cout<<"     Core is not set yet! You must first set the core position! Exiting..."<<endl;
	exit(-1);
    }

    if(aArrayCoreCS.size()>0)
    {
	cout<<endl<<"(WW) AntennaArray::CalculateArrayCoreCS warning:"<<endl;
	cout<<"     aArrayCoreCS not empty. Reseting it..."<<endl;
	aArrayCoreCS.clear();
	ArrayCoreCSIsSet=false;
    }

    int n=aArray.size();
    
    
    Vector3D vant;
    ArrayAntenna ant;
    
    for(int i=0;i<n;i++)
    {
	vant.SetCart(aArray[i].x,aArray[i].y,aArray[i].z);
	Vector3D vantCoreCS=vant-Core;
	
	//Id is unchanged
	ant.Id=aArray[i].Id;
	
	//Set new calculated coordinates
	ant.x=vantCoreCS.x;
	ant.y=vantCoreCS.y;
	ant.z=vantCoreCS.z;
	
	//add to CoreCs array
	aArrayCoreCS.push_back(ant);

	if(Verbose) cout<<"x: "<<aArray[i].x<<" -> "<<ant.x<<"\t y: "<<aArray[i].y<<" -> "<<ant.y<<"\t z: "<<aArray[i].z<<" -> "<<ant.z<<endl;

    }

    stringstream s;
    s<<"Array-D"<<D<<"m-L"<<Length<<"m-W"<<Width<<"m-Corex_"<<Core.x<<"m-y_"<<Core.y<<"m-z_"<<Core.z<<"m-ArrayCoreCS.dat";
    ArrayCoreCSFileName=s.str();

    ArrayCoreCSIsSet=true;
    ArrayCoreCSWasTransformed=false;

    if(Verbose) cout<<endl<<"Array CoreCS is set! Default output file name is: "<<endl<<ArrayCoreCSFileName<<endl;
    
    return;
    
    

}

//calculates the Psi angles for the antennas in the aArrayCoreCS vector only
void AntennaArray::CalculatePsi()
{
    if(!ArrayCoreCSIsSet)
    {
	cout<<endl<<"(EE) AntennaArray::CalculatePsi error:"<<endl;
	cout<<"     Array (CoreCS) is not set yet! You must first set the array by setting geometry and core! Exiting..."<<endl;
	exit(-1);
    }
    if(!XmaxIsSet)
    {
	cout<<endl<<"(EE) AntennaArray::CalculatePsi error:"<<endl;
	cout<<"     Xmax not set yet! Cannot calculate Psi angles! Exiting..."<<endl;
	exit(-1);
    }

    
    int n=aArrayCoreCS.size();
    
    
    Vector3D Prop;    //vector from Xmax to Antenna
    Vector3D Ant;     //vector from core to Antenna
    
    Vector3D Axis(-Xmax.x,-Xmax.y,-Xmax.z,true);
    
    double pi=acos(-1);

    double antx,anty,antz;  //antenna coorsinates
    double Psi;
    for(int i=0;i<n;i++)
    {
	antx=aArrayCoreCS[i].x;
	anty=aArrayCoreCS[i].y;
	antz=aArrayCoreCS[i].z;
	
	//Set antenna vector3d
	Ant.SetCart(antx,anty,antz);
	
	//Calculate propagation (Vector3D)
	Prop=Axis+Ant;
	
	
	Psi=VecAngle(Axis,Prop);
	//If shower is "upgoing", Psi=180deg-Psi
	if(ShowerIsUpgoingInCoreCS) Psi=pi-Psi;
	
	cout<<"antx="<<i<<" Psi="<<Psi*180./pi<<endl;

	//set antenna Psi
	aArrayCoreCS[i].Psi=Psi;
    }
    
    return;
   
}

void AntennaArray::RotateArrayCoreCS(double theta,double phi)
{

    if(!ArrayCoreCSIsSet)
    {
	cout<<endl<<"(EE) AntennaArray::RotateArrayCoreCS error:"<<endl;
	cout<<"     Array (CoreCS) is not set yet! You must first set the array by setting geometry and core! Exiting..."<<endl;
	exit(-1);
    }
    
    double antx,anty,antz;  //antenna coorsinates
    
    int n=aArrayCoreCS.size();
    
    Vector3D vin,vout;
    

    for (int i=0;i<n;i++)
    {
    
	antx=aArrayCoreCS[i].x;
	anty=aArrayCoreCS[i].y;
	antz=aArrayCoreCS[i].z;
	
	vin.SetCart(antx,anty,antz);
	vout=Rotate(theta,phi,vin);
	
	//Set new antenna position and reset Psi
	aArrayCoreCS[i].x=vout.x;
	aArrayCoreCS[i].y=vout.y;
	aArrayCoreCS[i].z=vout.z;
	aArrayCoreCS[i].Psi=-1; //reset Psi
	
    }
    ArrayCoreCSWasTransformed=true;
    
}



void AntennaArray::RotateArrayCoreCSDeg(double thetadeg,double phideg)
{
    double pi=acos(-1), d2r=pi/180.;
    double theta=thetadeg*d2r;
    double phi=phideg*d2r;

    RotateArrayCoreCS(theta,phi);
    
}


void AntennaArray::TranslateArrayCoreCS(double x, double y, double z)
{
    if(!ArrayCoreCSIsSet)
    {
	cout<<endl<<"(EE) AntennaArray::TranslateArrayCoreCS error:"<<endl;
	cout<<"     Array (CoreCS) is not set yet! You must first set the array by setting geometry and core! Exiting..."<<endl;
	exit(-1);
    }
    
    double antx,anty,antz;  //antenna coorsinates
    
    int n=aArrayCoreCS.size();
    
    for (int i=0;i<n;i++)
    {
    	aArrayCoreCS[i].x+=x;
	aArrayCoreCS[i].y+=y;
	aArrayCoreCS[i].z+=z;
	aArrayCoreCS[i].Psi=-1; //reset Psi
    }
    ArrayCoreCSWasTransformed=true;
}

void AntennaArray::TranslateArrayCoreCS(Vector3D vt)
{
    double x=vt.x;
    double y=vt.y;
    double z=vt.z;
    TranslateArrayCoreCS(x,y,z);
}


void AntennaArray::CreatePerpendicularArrayCoreCS(double dxmax)
{
    if(!ArrayCoreCSIsSet)
    {
	cout<<endl<<"(EE) AntennaArray::CreatePerpendicularArrayCoreCS error:"<<endl;
	cout<<"     Array (CoreCS) is not set yet! You must first set the array by setting geometry and core! Exiting..."<<endl;
	exit(-1);
    }

    if(!XmaxIsSet)
    {
	cout<<endl<<"(EE) AntennaArray::CreatePerpendicularArrayCoreCS error:"<<endl;
	cout<<"     Xmax is not set yet! You must first set Xmax to define the shower axis!Exiting..."<<endl;
	exit(-1);
    }

    cout<<"Creating a perpendicular array..."<<endl;
    double pi=acos(-1),r2d=180./pi;

    double theta=Xmax.theta;
    double phi=Xmax.phi;
    
    cout<<"Xmax: theta="<<theta*r2d<<" phi="<<phi*r2d<<"  rotating array...."<<endl;

    RotateArrayCoreCS(-theta,phi);

    Vector3D vtranslate; //Will translate array using this vector
      
    //at first it is Xmax (that would be translating the array to Xmax)
    vtranslate=Xmax;

    //now we change the ampplitude of vtranslate (depending on up or downgoing)
    double newr;
    if(ShowerIsUpgoingInCoreCS) newr=Xmax.r+dxmax;
    else newr=Xmax.r-dxmax;

    //change amplitude of vtranslate according to newr
    vtranslate.SetSpher(newr,vtranslate.theta,vtranslate.phi);
    
    
    TranslateArrayCoreCS(vtranslate);

    ArrayCoreCSWasTransformed=true;

  
}


void AntennaArray::FilterArray()
{
    if(!ArrayCoreCSIsSet)
    {
	cout<<endl<<"(EE) AntennaArray::FilterArray error:"<<endl;
	cout<<"     Array (CoreCS) is not set yet! You must first set the array by setting geometry and core! Exiting..."<<endl;
	exit(-1);
    }

    double r2d=180./acos(-1);

    cout<<endl<<"Filtering array: MinZ="<<MinZ<<", MaxZ="<<MaxZ<<", MinPsi="<<MinPsi*r2d<<" and MaxPsi="<<MaxPsi*r2d<<endl; 

    int n=aArrayCoreCS.size();
    for(int i=0;i<n;i++)
    {
	if(  (aArrayCoreCS[i].z >= MinZ) && (aArrayCoreCS[i].z <= MaxZ)  && (aArrayCoreCS[i].Psi >= MinPsi) &&  (aArrayCoreCS[i].Psi <= MaxPsi) ) 
	{
	    aArrayFiltered.push_back(aArrayCoreCS[i]);
	}

    }

    
   
   
    
    if(aArrayFiltered.size() == 0)
    {
	cout<<endl<<"(WW) AntennaArray::FilterArray warning:"<<endl;
	cout<<"     No antennas passed the cuts! Filtered array was not set!"<<endl;
    }
    else
    {
	//set output file name
	stringstream s;
	s<<"Array-D"<<D<<"m-L"<<Length<<"m-W"<<Width<<"m-Corex_"<<Core.x<<"m-y_"<<Core.y<<"m-z_"<<Core.z<<"m-Filtered.dat";
	ArrayFilteredFileName=s.str();
	
	ArrayFilteredIsSet=true;
    }

}





////////////////////////////////////////////////////////////////
//Dump and message methods   /////////////////////////////////// 
////////////////////////////////////////////////////////////////

void AntennaArray::Hello()
    {
	cout<<endl;
	cout<<"********************************"<<endl;
	cout<<"********************************"<<endl;
	cout<<"**      AntennaArray v"<<VERSIONAA<<"     **"<<endl;
	cout<<"********************************"<<endl;
	cout<<"********************************"<<endl;
	cout<<endl;
	cout<<endl;
    }


void AntennaArray::WriteArrayFile()
{
    if(!ArrayIsSet)
    {
	cout<<endl<<"(WW) AntennaArray::WriteArrayFile warning: Array is not set yet! Did NOT write any files!"<<endl;
	return;
    }

    cout<<endl<<"Array file name is set to "<<ArrayFileName<<endl;

    //test if output file exists and give options (interactive) ////////
    ////////////////////////////////////////////////////////////////////

    while( FileExists(ArrayFileName) )
    {
	cout<<endl<<"File Exists! Do you want to overwrite it? y/n"<<endl;
	char answer;
	cin>>answer;
	if (answer=='n' || answer=='N')
	{
	    cout<<endl<<"Do you want to change the file name?"<<endl;  
	    cin>>answer;
	    if (answer=='n' || answer=='N')
	    {
		cout<<"Ok! Did not write any files!"<<endl;
		return;
	    }
	    else 
	    {
		string newname;
		cout<<"Enter new full file path to use:"<<endl;
		cin>>newname;
		SetArrayFileName(newname);
		cout<<endl<<"Array file name (array CS) is now set to "<<ArrayFileName<<endl;
	    }
	}
	else 
	{
	    cout<<endl<<"Will overwrite file "<<ArrayFileName<<"."<<endl;
	    break;
	}
	
    }
    ////////////////////////////////////////////////////////////////////
    //////////////////// End test if file exists ///////////////////////

    
    //Writing file
    ofstream file(ArrayFileName.c_str());

    cout<<"Writing..."<<endl;
//header
    file<<"# /////////////////////////////////////////////////"<<endl;
    file<<"# ////  File created by AntennaArray v"<<VERSIONAA<<"  ////"<<endl;
    file<<"# ///////////////////////////////////////////////"<<endl;
    file<<"#"<<endl;
    file<<"#  1: Antenna Id"<<endl;
    file<<"#  2: Antenna x coordinate (m)"<<endl;
    file<<"#  3: Antenna y coordinate (m)"<<endl;
    file<<"#  4: Antenna z coordinate (m)"<<endl;
    file<<"#  5: Antenna angle Psi (deg) value is negative if not set yet"<<endl;
    
    file<<"#"<<endl;
   
    //WriteHeader(file);
    
    double pi=acos(-1);
    double r2d=180./pi;

    int nants=aArray.size();
    for(int i=0;i<nants;i++)
    {
	file<<aArray[i].Id<<"\t"<<aArray[i].x<<"\t"<<aArray[i].y<<"\t"<<aArray[i].z<<"\t"<<aArray[i].Psi*r2d<<endl;
    }

    cout<<"done!"<<endl;
       
}




void AntennaArray::WriteArrayCoreCSFile()
{
    if(!ArrayCoreCSIsSet)
    {
	cout<<endl<<"(WW) AntennaArray::WriteArrayCoreCSFile warning: Array is not set yet in the core system! Did NOT write any files!"<<endl;
	return;
    }

    
    cout<<endl<<"Array CoreCS file name is set to "<<ArrayCoreCSFileName<<endl;

    //test if output file exists and give options (interactive) ////////
    ////////////////////////////////////////////////////////////////////

    while( FileExists(ArrayCoreCSFileName) )
    {
	cout<<endl<<"File Exists! Do you want to overwrite it? y/n"<<endl;
	char answer;
	cin>>answer;
	if (answer=='n' || answer=='N')
	{
	    cout<<endl<<"Do you want to change the file name?"<<endl;  
	    cin>>answer;
	    if (answer=='n' || answer=='N')
	    {
		cout<<"Ok! Did NOT write any files!"<<endl;
		return;
	    }
	    else 
	    {
		string newname;
		cout<<"Enter new full file path to use:"<<endl;
		cin>>newname;
		SetArrayCoreCSFileName(newname);
		cout<<endl<<"Array file name (Core CS) is now set to "<<ArrayCoreCSFileName<<endl;
	    }
	}
	else 
	{
	    cout<<endl<<"Will overwrite file "<<ArrayCoreCSFileName<<"."<<endl;
	    break;
	}
	
    }
    ////////////////////////////////////////////////////////////////////
    //////////////////// End test if file exists ///////////////////////

    
    //Writing file
    ofstream file(ArrayCoreCSFileName.c_str());

    cout<<"Writing..."<<endl;
//header
    file<<"# /////////////////////////////////////////////////"<<endl;
    file<<"# ////  File created by AntennaArray v"<<VERSIONAA<<"  ////"<<endl;
    file<<"# ///////////////////////////////////////////////"<<endl;
    file<<"#"<<endl;
    file<<"#  1: Antenna Id"<<endl;
    file<<"#  2: Antenna x coordinate (m)"<<endl;
    file<<"#  3: Antenna y coordinate (m)"<<endl;
    file<<"#  4: Antenna z coordinate (m)"<<endl;
    file<<"#  5: Antenna angle Psi (deg) value is negative if not set yet"<<endl;
    file<<"#"<<endl;
    file<<"#    Core was set to ("<<Core.x<<", "<<Core.y<<", "<<Core.z<<") m"<<endl;
    file<<"#"<<endl;
   
    //WriteHeader(file);
    
    double pi=acos(-1);
    double r2d=180./pi;

    int nants=aArrayCoreCS.size();
    for(int i=0;i<nants;i++)
    {
	file<<aArrayCoreCS[i].Id<<"\t"<<aArrayCoreCS[i].x<<"\t"<<aArrayCoreCS[i].y<<"\t"<<aArrayCoreCS[i].z<<"\t"<<aArrayCoreCS[i].Psi*r2d<<endl;
    }

    cout<<"done!"<<endl;
       
}




void AntennaArray::WriteArrayFilteredFile()
{
    if(!ArrayFilteredIsSet)
    {
	cout<<endl<<"(WW) AntennaArray::WriteArrayFilteredFile warning: Filtered array is not set yet! Did NOT write any files!"<<endl;
	return;
    }

    
    cout<<endl<<"Filtered array file name is set to "<<ArrayFilteredFileName<<endl;

    //test if output file exists and give options (interactive) ////////
    ////////////////////////////////////////////////////////////////////

    while( FileExists(ArrayFilteredFileName) )
    {
	cout<<endl<<"File Exists! Do you want to overwrite it? y/n"<<endl;
	char answer;
	cin>>answer;
	if (answer=='n' || answer=='N')
	{
	    cout<<endl<<"Do you want to change the file name?"<<endl;  
	    cin>>answer;
	    if (answer=='n' || answer=='N')
	    {
		cout<<"Ok! Did NOT write any files!"<<endl;
		return;
	    }
	    else 
	    {
		string newname;
		cout<<"Enter new full file path to use:"<<endl;
		cin>>newname;
		SetArrayFilteredFileName(newname);
		cout<<endl<<"Filtered array file name is now set to "<<ArrayFilteredFileName<<endl;
	    }
	}
	else 
	{
	    cout<<endl<<"Will overwrite file "<<ArrayFilteredFileName<<"."<<endl;
	    break;
	}
	
    }
    ////////////////////////////////////////////////////////////////////
    //////////////////// End test if file exists ///////////////////////
    double pi=acos(-1);
    double r2d=180./pi;
    
    //Writing file
    ofstream file(ArrayFilteredFileName.c_str());

    cout<<"Writing..."<<endl;
//header
    file<<"# /////////////////////////////////////////////////"<<endl;
    file<<"# ////  File created by AntennaArray v"<<VERSIONAA<<"  ////"<<endl;
    file<<"# ///////////////////////////////////////////////"<<endl;
    file<<"#"<<endl;
    file<<"#  1: Antenna Id"<<endl;
    file<<"#  2: Antenna x coordinate (m)"<<endl;
    file<<"#  3: Antenna y coordinate (m)"<<endl;
    file<<"#  4: Antenna z coordinate (m)"<<endl;
    file<<"#  5: Antenna angle Psi (deg) value is negative if not set yet"<<endl;
    file<<"#"<<endl;
    file<<"#    Core was set to ("<<Core.x<<", "<<Core.y<<", "<<Core.z<<") m"<<endl;
    file<<"#"<<endl;
    file<<"#    Filters: MinZ="<<MinZ<<" MaxZ="<<MaxZ<<" MinPsi="<<MinPsi*r2d<<" MaxPsi="<<MaxPsi*r2d<<endl;
    file<<"#"<<endl;
   
    //WriteHeader(file);
    
    

    int nants=aArrayFiltered.size();
    for(int i=0;i<nants;i++)
    {
	file<<aArrayFiltered[i].Id<<"\t"<<aArrayFiltered[i].x<<"\t"<<aArrayFiltered[i].y<<"\t"<<aArrayFiltered[i].z<<"\t"<<aArrayFiltered[i].Psi*r2d<<endl;
    }

    cout<<"done!"<<endl;
       
}

