#include "NuDecay_ROOT.h"
#include "NuSimPreparator.h"
#include "AntennaArray.h"

int main()
{
    //Pars
    string name="NuDecaysE1e17.root",empty;
    int decayn=1238;
    double thetadeg=89.8;
    double phideg=180.;
    double groundaltitude=2650.;
    double decayheight=1.;
    double desiredtauenergy=1e17;
    double thinningenergy=1.e-4;
    double dxmaxgcm2=500.0;
    double MaxAngleInDeg=1.0;
    double MaxAngleOutDeg=2.4;
    //array
    double d=300.;
    double length=100000.; //was 80000
    double width=11000.;
    
    
    // cout<<"TEST 1:"<<endl;
    // NuSimPreparator event0;
    // event0.SetUsePlaneEarthOn();
    
    

    // event0.SetShowerGeometry(thetadeg,phideg,decayheight,groundaltitude);
    // event0.SetDXmaxgcm2(1000);
    // cout<<"Dxmax="<<event0.DXmax<<endl;
    // //induce error tests /////

    // //event0.CreateSpecialParticleFile();
    // //event0.CreateAiresInpFile();

    // //////////////////////////  


    // event0.SetDesiredTauEnergy(desiredtauenergy);
    // event0.SetDecayN(decayn);
    // event0.SetDecayInputFile(name);

    // event0.ReadDecay();
    // //event0.DumpShowerGeometry();
    // //event0.DumpDecayInput();
    // //event0.DumpParticleArray();
    



    // cout<<endl<<"TEST 2:"<<endl;
    // NuSimPreparator event(thetadeg,phideg,decayheight,groundaltitude);
    // event.SetDesiredTauEnergy(desiredtauenergy);
    // event.SetDecayN(decayn);
    // event.SetDecayInputFile(name);

    // event.ReadDecay();
    // //event.DumpShowerGeometry();
    // //event.DumpDecayInput();
    // //event.DumpParticleArray();


    cout<<endl<<"TEST 3:"<<endl;
    NuSimPreparator event2(desiredtauenergy,thetadeg,phideg,decayheight,groundaltitude,name,decayn);
    event2.ReadDecay();
    event2.SetThinningEnergy(thinningenergy);

    event2.SetVerboseOn();
    event2.SetDXmaxgcm2(dxmaxgcm2);
    event2.SetMaxAngleInDeg(MaxAngleInDeg);
    event2.SetMaxAngleOutDeg(MaxAngleOutDeg);
    //event2.SetFilterArrayOff();
    //event2.Array.SetVerboseOff();

    //NO CUT IN PSI: CALCULATE FULL ARRAY //////
    //event2.SetFilterArrayOff();
    ////////////////////////////////////////////

    event2.CreateAntennaArray(d, length, width);
    
    event2.Array.WriteArrayCoreCSFile();

    event2.CreateSpecialParticleFile();
    event2.CreateAiresInpFile();
    event2.DumpParticleArray();
    AntennaArray array;

    
	
}
