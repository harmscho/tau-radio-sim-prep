/* Simplpe library to generate input files and scripts to run zhaires with neutrino primaries (special particle).

Washington Carvalho Apr 2015
*/

#include "NuSimPreparator.h"



////////////////////////////////////////////////////////////
//CONSTRUCTORS   ///////////////////////////////////////////
////////////////////////////////////////////////////////////



//default constructor
NuSimPreparator::NuSimPreparator():
ShowerGeometryIsSet(false),
DecayIsSet(false),
User("washington.carvalho"),
Queue("auger.q"),
Priority(0),
UseRoot(true),
DecayInputFile(""),
DecayN(-1),
Verbose(true),
Version(VERSIONSP),
DesiredTauEnergy(-1),
ThinningEnergy(1.e-6),
TaskName(""),
UsePlaneEarth(false),
SpecialParticleFileWritten(false),
AiresInpFileWritten(false),
RandomSeed(0.1218),
stepm(5.),
precision(0.1),
DXmax(1000.),
MaxAngleInDeg(0.5),
MaxAngleOutDeg(0.8),
FilterArray(true),
ArrayIsSet(false),
R0(325.),
kr(0.1218),
TimeDomainBin(0.3),
GeomagneticFielduT(56.0),
GeomagneticInclinationDeg(63.5),
InvertPhi(true)
{
    Hello();
}
//

//Constructor that sets the "downgoing" shower geometry in degrees
NuSimPreparator::NuSimPreparator(double thetadeg, double phideg, double decayheight, double groundaltitude):
ShowerGeometryIsSet(false),
DecayIsSet(false),
User("washington.carvalho"),
Queue("auger.q"),
Priority(0),
UseRoot(true),
DecayInputFile(""),
DecayN(-1),
Verbose(true),
Version(VERSIONSP),
DesiredTauEnergy(-1),
ThinningEnergy(1.e-6),
TaskName(""),
UsePlaneEarth(false),
SpecialParticleFileWritten(false),
AiresInpFileWritten(false),
RandomSeed(0.1218),
stepm(5.),
precision(0.1),
DXmax(1000.),
MaxAngleInDeg(0.5),
MaxAngleOutDeg(0.8),
FilterArray(true),
ArrayIsSet(false),
R0(325.),
kr(0.1218),
TimeDomainBin(0.3),
GeomagneticFielduT(56.0),
GeomagneticInclinationDeg(63.5),
InvertPhi(true)
{
    Hello();
    SetShowerGeometry(thetadeg,phideg,decayheight,groundaltitude);
}

//Constructor that sets the "downgoing" shower geometry in degrees
NuSimPreparator::NuSimPreparator(double desiredtauenergy, double thetadeg, double phideg, double decayheight, double groundaltitude, string inputfile, int decayn):
ShowerGeometryIsSet(false),
DecayIsSet(false),
User("washington.carvalho"),
Queue("auger.q"),
Priority(0),
UseRoot(true),
DecayInputFile(""),
DecayN(-1),
Verbose(true),
Version(VERSIONSP),
DesiredTauEnergy(-1),
ThinningEnergy(1.e-6),
TaskName(""),
UsePlaneEarth(false),
SpecialParticleFileWritten(false),
AiresInpFileWritten(false),
RandomSeed(0.1218),
stepm(5.),
precision(0.1),
DXmax(1000.),
MaxAngleInDeg(0.5),
MaxAngleOutDeg(0.8),
FilterArray(true),
ArrayIsSet(false),
R0(325.),
kr(0.1218),
TimeDomainBin(0.3),
GeomagneticFielduT(56.0),
GeomagneticInclinationDeg(63.5),
InvertPhi(true)
{
    Hello();
    
    SetShowerGeometry(thetadeg,phideg,decayheight,groundaltitude);
    
    //Set decay input variables
    SetDecayInputFile(inputfile);
    
    SetDecayN(decayn);

    SetDesiredTauEnergy(desiredtauenergy);

    if(!CheckDecayInput())
    {
	cout<<endl<<"(EE) NuSimPreparator constructor error:"<<endl;
	cout<<"     Decay input parameters not set correctly!"<<endl;
	cout<<"     Empty filename or DecayN<1."<<endl;
	cout<<"     Input parameters used: "<<endl;
	cout<<"     Decay input file:    "<<DecayInputFile<<endl;
	cout<<"     Decay number to use: "<<DecayN<<endl;
	cout<<"     Desired Tau Energy:  "<<DesiredTauEnergy<<endl;
	cout<<"     Exiting..."<<endl;
	exit(-1);
    }
    
    if(Verbose) DumpDecayInput();
}


//Constructor that sets the "downgoing" shower geometry in degrees
NuSimPreparator::NuSimPreparator(double desiredtauenergy, double thetadeg, double phideg, double decayheight, double groundaltitude, string inputfile, int decayn, bool invertphi):
ShowerGeometryIsSet(false),
DecayIsSet(false),
User("washington.carvalho"),
Queue("auger.q"),
Priority(0),
UseRoot(true),
DecayInputFile(""),
DecayN(-1),
Verbose(true),
Version(VERSIONSP),
DesiredTauEnergy(-1),
ThinningEnergy(1.e-6),
TaskName(""),
UsePlaneEarth(false),
SpecialParticleFileWritten(false),
AiresInpFileWritten(false),
RandomSeed(0.1218),
stepm(5.),
precision(0.1),
DXmax(1000.),
MaxAngleInDeg(0.5),
MaxAngleOutDeg(0.8),
FilterArray(true),
ArrayIsSet(false),
R0(325.),
kr(0.1218),
TimeDomainBin(0.3),
GeomagneticFielduT(56.0),
GeomagneticInclinationDeg(63.5)
{
    Hello();

    InvertPhi=invertphi;

    SetShowerGeometry(thetadeg,phideg,decayheight,groundaltitude);
    
    //Set decay input variables
    SetDecayInputFile(inputfile);
    
    SetDecayN(decayn);

    SetDesiredTauEnergy(desiredtauenergy);

    if(!CheckDecayInput())
    {
	cout<<endl<<"(EE) NuSimPreparator constructor error:"<<endl;
	cout<<"     Decay input parameters not set correctly!"<<endl;
	cout<<"     Empty filename or DecayN<1."<<endl;
	cout<<"     Input parameters used: "<<endl;
	cout<<"     Decay input file:    "<<DecayInputFile<<endl;
	cout<<"     Decay number to use: "<<DecayN<<endl;
	cout<<"     Desired Tau Energy:  "<<DesiredTauEnergy<<endl;
	cout<<"     Exiting..."<<endl;
	exit(-1);
    }
    
    if(Verbose) DumpDecayInput();
}


///////////////////////////////////////////////////////////
// Set Methods/////////////////////////////////////////////
///////////////////////////////////////////////////////////


void NuSimPreparator::SetDesiredTauEnergy(double desiredtauenergy)
{
    if (desiredtauenergy < atof(MINE) || desiredtauenergy > atof(MAXE) )
    {
	cout<<endl<<"(EE) NuSimPreparator::SetDesiredTauEnergy::"<<endl;
	cout<<"     DesiredTauEnergy must be betwee "<<MINE<<" and "<<MAXE<<" eV"<<endl;
    }
    DesiredTauEnergy=desiredtauenergy;
}

void NuSimPreparator::SetShowerGeometry(double thetadeg, double phideg, double decayheight, double groundaltitude)
{
    //check angles!
    if( !CheckAngles(thetadeg,phideg) )
    {
	cout<<endl<<"(EE) NuSimPreparator constructor error:"<<endl;
	cout<<"     Zenithal angle must in the range 0<=theta<90 degrees"<<endl;
	cout<<"     Azimuthal angle must be in the range 0<=phi<360 deg. Exiting..."<<endl;
	cout<<"     Input values were: theta="<<thetadeg<<"deg and phi="<<phideg<<" deg."<<endl;
	exit(-1);
    }
    
    //Sets "real" downgoing shower angles
    ThetaDeg=thetadeg;
    Theta=ThetaDeg*d2r;
    PhiDeg=phideg;
    Phi=PhiDeg*d2r;

    //sets Aires angles (downgoing) and shower coming from opposite direction
    //This sets the "fake" shower axis that AIRES will use 
    //(this is a workaround for the time window calculations in zhaires)
   
    AiresThetaDeg=ThetaDeg;
    AiresTheta=AiresThetaDeg*d2r;

    if(InvertPhi)
    {
	//Inverts phi: for use with regular zhaires and antennas on the ground
	AiresPhiDeg=PhiDeg+180.;
	if(AiresPhiDeg >=360) AiresPhiDeg-=360.;
	AiresPhi=AiresPhiDeg*d2r;
    }
    else
    {
	//Does not invert phi: A special "upgoing shower" zhaires version must 
	//be used. 
	AiresPhiDeg=PhiDeg;
	AiresPhi=AiresPhiDeg*d2r;
    }
    

    //Helper/speedup variables (based on the real downgoing axis angles!!!)
    CosTet=cos(Theta);
    CosPhi=cos(Phi);
    SinTet=sin(Theta);
    SinPhi=sin(Phi);
    //cout<<"CosTet="<<CosTet<<" SinTet="<<SinTet<<"  Cosphi="<<CosPhi<<" SinPhi="<<SinPhi<<endl;
    
    //Set decay height and ground altitude
    SetDecayHeight(decayheight);
    SetGroundAltitude(groundaltitude);


    //Decay point calculation /////////////////////////
    ///////////////////////////////////////////////////

    
    //case 1: Plane Earth /////////////////////
    if(UsePlaneEarth)
    {
	if(Verbose) cout<<endl<<"Using plane Earth approximation to calculate decay point coordinates."<<endl;
	
	//calculate decay distance and its projection on the ground
	//("vertical" right triangle containing shower core and injection point)
	DecayDistance=DecayHeight/CosTet;       //hypotenuse
	DecayGroundDistance=DecayDistance*SinTet; //Projection of hypotenuse on ground
	//calculate injection point in the AIRES system (origin is at sea level)
	Xinj=DecayGroundDistance*CosPhi;
	Yinj=DecayGroundDistance*SinPhi;
	Zinj=DecayHeight+GroundAltitude;

	//Decay point (Injection point) in Core CS
	vDecay.SetCart(Xinj,Yinj,DecayHeight);
	vAxis=vDecay;
	vAxis.MakeUnit();
	// end case 1: Plane Earth ////////////////
    }
    else
    {
	if(Verbose) cout<<endl<<"Using full curved Earth calculation of decay point coordinates."<<endl;
	//case 2: Curved Earth ////////////////// 
	double Rt=6.371e6;      //earth radius in m

	//We use a corrected radius (includes ground altitude)
	double Rtc=Rt+GroundAltitude;
	
	//2nd degree equation in Distance D from the decay point to core
	//Note that DecayGroundDistance (r below) is the projection of the 
	//shower axis from the decay point to the core on a PLANE
	//perpendicular to the z axis, e.g. the plane at height z above 
	//ground, and not the actual projection on the curved surface of
	//the Earth.
	//In this geometry, z of the decay point is no longer just
        //the decay height (h below)

	//Eq: (Rtc+h)**2 = (Rtc+z)**2 + r**2, where
	//    r=D*sin(theta)  and z=D*cos(theta)
	//we use shorter variable names below just to make it more clear
	
	double A=1;
	double B=2*Rtc*CosTet;
	double C=-(DecayHeight*DecayHeight)-2*Rtc*DecayHeight;
	double Delta=B*B-4*A*C;
	double D=(-B+sqrt(Delta))/2*A;

	double r=D*SinTet;
	double z=D*CosTet;
	
	DecayDistance=D;
	DecayGroundDistance=r;
	
	//Decay point (Injection point) in AIRES system (origin at sea level)
	Xinj=DecayGroundDistance*CosPhi;
	Yinj=DecayGroundDistance*SinPhi;
	Zinj=z+GroundAltitude;

	//Decay point (Injection point) in Core CS 
	//(decay point in AIRES CS translated by DecayHeight)
	vDecay.SetCart(Xinj,Yinj,z); //z, not Zinj!

	//decay time
	//The decay time is set so that a plane shower front (that is actually 
	//upgoing) would cross the Core at t=0 (i.e. even before the injection 
	//time, so this front actually does not exist in t=0). 

	double c=2.99792458e8; //c in m/s
	DecayTime=vDecay.r/c*1.e9;  //ns
	
	//Set "real" axis direction (UPGOING in this case)
	//This would be the z-axis of the "decay" coord. system
	vAxis=vDecay;
	vAxis.MakeUnit();
    }
    
    //Shower geometry is set!
    ShowerGeometryIsSet=true;
    if(Verbose) DumpShowerGeometry();
}



void NuSimPreparator::SetDXmaxgcm2(double T)
{
    if( !ShowerGeometryIsSet )
    {
	cout<<endl<<"(EE) NuSimPreparator::SetDXmaxgcm2 error:"<<endl;
	cout<<"     Shower geometry is not set yet! Cannot calculate Dxmax!"<<endl;
	return;	
    }   

    //NOTE: For now this calculation uses a plane earth approximation
    //i.e. it disregrads the UsePlaneEarth flag 
    
    double alt0m=Zinj;  //altitude of decay
    double D=0;  //Dxmax that we will calculate

    double step=stepm*100.; //step in cm
    double stepkm=stepm*1.e-3; //step in km
    double alt0km=alt0m/1.e3; //alt0 in km

    //set direction (real shower axis)
    Vector3D vDir(1.,Theta,Phi,false);
    

    float alt, densitykgm3, density, dummy1, dummy2;

    double deltaAltkm=stepkm*vDir.z; //variation in altitude for each step in km
    
    if(Verbose) cout<<endl<<"DXmax calculation:"<<endl<<"stepcm="<<step<<" cm stepkm="<<stepkm<<" km alt0m="<<alt0m<<" m alt0km="<<alt0km<<" km deltaAltkm="<<deltaAltkm<<" km"<<endl<<"Vdir:"<<endl;

    vDir.Print();
    double stepX;  //atmospheric thickness for the current step

    double sum=0;   //addition of thickness of steps
    

    bool done=false;
    double tempsum;

    int i=1;
    double lastalt=alt0km-deltaAltkm/2;  //half a step below Decay
    while(!done)
    {
	//get altitude of middle of step
	alt=lastalt + deltaAltkm;  //altitude at middle of step in km
	//get density for this altitude (middle of step
	Atmosphere(alt,densitykgm3,dummy1,dummy2);
	density=densitykgm3/1.e3;
	
	stepX=density*step;
	tempsum=stepX+sum;

	if( tempsum < T && fabs(T-tempsum)>precision) //not done yet!
	{
	    
	    sum+=stepX;
	    D+=step/100.; //in m (step is in cm!)
	    lastalt=alt;
	    i++;
	    if(Verbose) cout<<"Step "<<i-1<<": alt="<<alt<<"km density="<<density<<" StepX="<<stepX<<" sum="<<sum<<endl;
	    
	}
	else if ( fabs(T-tempsum)<=precision ) //done!
	{
	    sum+=stepX;
	    D+=step/100.; //in m (step is in cm!)
	    done=true;

	    if(Verbose) cout<<"Done! T="<<T<<" g/cm2  precision="<<precision<<" g/cm2 sum="<<sum<<" g/cm2  D="<<D<<"m i="<<i<<endl;
	}
	else  //overshot T !!! reduce step by half and try again
	{
	    step/=2.;  //halve the step
	    deltaAltkm/=2.;
	    if(Verbose)cout<<"overshot! ("<<tempsum<<" gcm2). Halving the step..."<<endl;

	}

	
    }

    SetDXmax(D);

}



void NuSimPreparator::SetDecayN(int decayn)
{
    if(decayn<1)
    {
	cout<<endl<<"(EE) NuSimPreparator::SetDecayN error:"<<endl;
	cout<<"      Input parameter "<<decayn<<" not greater than 0! Exiting..."<<endl;
	exit(-1);
    }
    
    DecayN=decayn;
}
void NuSimPreparator::SetDecayInputFile(string inputfile)
{
    if(inputfile=="")
    {
	cout<<"(EE)  NuSimPreparator::SetDecayInputFile error:"<<endl;
	cout<<"      Empty decay input filename! Exiting..."<<endl;
	exit(-1);
    }
    DecayInputFile=inputfile;
}

void NuSimPreparator::SetDecayHeight(double decayheight)
{
    if(decayheight<0)
    {
	cout<<"(EE)  NuSimPreparator::SetDecayHeight error:"<<endl;
	cout<<"      Decay height must be positive! Exiting..."<<endl;
	exit(-1);
    }
    DecayHeight=decayheight;

}



void NuSimPreparator::SetGroundAltitude(double groundaltitude)
{
    if(groundaltitude<0)
    {
	cout<<"(EE)  NuSimPreparator::SetGroundAltitude error:"<<endl;
	cout<<"      GroundAltitude must be positive! Exiting..."<<endl;
	exit(-1);
    }
    GroundAltitude=groundaltitude;

}

void  NuSimPreparator::SetThinningEnergy(double thinningenergy)
{
    if( thinningenergy>atof(MAXTHINNING) || thinningenergy<atof(MINTHINNING) )
    {
	cout<<endl<<"(EE) NuSimPreparator::SetThinningEnergy error: Thinning Energy must be between "<<MINTHINNING<<" and "<<MAXTHINNING<<"! Exiting..."<<endl;
	exit(-1);
    }

    ThinningEnergy=thinningenergy;
}

void NuSimPreparator::SetTaskName(string taskname){TaskName=taskname;}

//Check methods  ///////////////////////////////////

bool NuSimPreparator::CheckAngles(double thetadeg, double phideg)
{
    if( thetadeg>90. || thetadeg<0 || phideg>=360 || phideg<0) return false;
    else return true;
}

bool NuSimPreparator::CheckDecayInput()
{
//test if DecayInputFile and DecayN are set
    if(DecayInputFile=="" || DecayN<1 || DesiredTauEnergy<0) return false;
    else return true;
}

bool NuSimPreparator::CheckParticleArray()
{
//test if DecayInputFile and DecayN are set
    if(pArray.size()==0) return false;
    else return true;
}




/////////////////////////////////////////////////////////////////////
// "Action" methods /////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////


//read decay from file (uses external reader struct/class e.g. NuDecay_ROOT
void NuSimPreparator::ReadDecay()
    {

       	//test if DecayInputFile and DecayN are set
	if(!CheckDecayInput())
	{
	    cout<<endl<<"(EE) NuSimPreparator::ReadDecay error: Input file for decay, decay number and/or desired tau energy are not set yet! Exiting...."<<endl;
	    exit(-1);
	}

	//Used for reading root files
	if(UseRoot)
	{

	    
	    
	    NuDecay_ROOT decay(DecayInputFile, DecayN);  //reads decay
	    
	    //get information from NuDecay reader....
	    TauEnergy=decay.GetTotalE();
	    ShowerEnergy=decay.GetShowerTotalE();
	    pArray=decay.GetpArray();

	    //Calculate the energy scaling factor and check for warning
	    EnergyScaleFactor=DesiredTauEnergy/TauEnergy;
	    if(EnergyScaleFactor>atof(MAXENERGYFACTOR) || EnergyScaleFactor<atof(MINENERGYFACTOR) )
	    {
		cout<<endl<<"(WW) NuSimPreparator::ReadDecay warning: Energy scale factor inside warning limits!"<<endl;
		cout<<"     DesiredTauEnergy: "<<DesiredTauEnergy<<" eV   TauEnergy (in file): "<<TauEnergy<<" eV  EnergyScaleFactor: "<<EnergyScaleFactor<<endl;
	    }
	    
	    

	    //Check if pArray is not empty
	    if( ! CheckParticleArray() )
	    {
		cout<<endl<<"(EE)  NuSimPreparator::ReadDecay error: Particle array from NuDecay_ROOT is empty! Exiting...."<<endl;
	    exit(-1);
	    }
	    
	    DecayIsSet=true;
	    
	    if(Verbose) DumpParticleArray();
	    


	}
	//used for reading "non-root" files (not yet implemented) 
	else
	{
	    //here will be the code for the "non-root" decay files
	    cout<<endl<<"(EE) NuDecay (non root) not implemented yet! Exiting..."<<endl;
	    exit(-1);
	}
    }




void NuSimPreparator::CreateSpecialParticleFile()
{
    if(!ShowerGeometryIsSet || !DecayIsSet)
    {
	cout<<endl<<"(EE) NuSimPreparator::CreateSpecialParticleFile error:"<<endl;
	cout<<"     Shower geometry or decay is not set yet! Exiting...."<<endl;
	exit(-1);	
    }

    
    if(TaskName == "")
    {
	std::ostringstream s;
	s <<"Nu-E"<<DesiredTauEnergy<<"eV-Tet"<<ThetaDeg<<"deg-Phi"<<PhiDeg<<"deg-h"<<DecayHeight<<"m-N"<<DecayN;
	TaskName=s.str();
	cout<<endl<<"TaskName was not manually set. Setting it to the default name: "<<endl;
	cout<<TaskName<<endl;
    }
    
    SpecialParticleFileName="SpecPcle-"+TaskName+".f";
    SpecialParticleExecutableFileName="SpecPcle-"+TaskName;
    cout<<"Special particle filename is set to: "<<SpecialParticleFileName<<endl;

    //test if output file exists and give options (interactive) ////////
    ////////////////////////////////////////////////////////////////////
    while( FileExists(SpecialParticleFileName) )
    {
	cout<<endl<<"File Exists! Do you want to overwrite it? y/n"<<endl;
	char answer;
	cin>>answer;
	if (answer=='n' || answer=='N')
	{
	    cout<<endl<<"Do you want to change TaskName?"<<endl;  
	    cin>>answer;
	    if (answer=='n' || answer=='N')
	    {
		cout<<"Ok! Exiting..."<<endl;
		exit(-1);
	    }
	    else 
	    {
		string newname;
		cout<<"Enter new TaskName to use:"<<endl;
		cin>>newname;
		SetTaskName(newname);
		SpecialParticleFileName="SpecPcle-"+TaskName+".f";
		SpecialParticleExecutableFileName="SpecPcle-"+TaskName;
		cout<<endl<<"New TaskName set to: "<<TaskName<<endl;
		cout<<"Special particle filename is set to: "<<SpecialParticleFileName<<endl;
		FileExists(SpecialParticleFileName);
		
	    }
	}
	else 
	{
	    cout<<endl<<"Will overwrite file "<<SpecialParticleFileName<<endl;
	    break;
	}
	    
    }
    ////////////////////////////////////////////////////////////////////
    //////////////////// End test if file exists ///////////////////////


    
    //Opening output file
    ofstream spout(SpecialParticleFileName.c_str());

    //Aires expects an integer version number for the macro
    //Will multiply "float" version number by 10 and transform to int
    int version=atof(VERSIONSP)*10;  //e.g. v1.1 -> version=11

    // Write Special particle File

    //// Write header and variable decalarations /////////////////////
    //////////////////////////////////////////////////////////////////


    cout<<endl<<"Writing special primary file...."<<endl;

    spout<<"c /////////////////////////////////////////////////"<<endl;
    spout<<"c ////  File created by NuSimPreparator v"<<VERSIONSP<<"  ////"<<endl;
    spout<<"c ///////////////////////////////////////////////"<<endl<<"c"<<endl<<"c"<<endl<<"c"<<endl;
    spout<<"      program NuDecaySpecialPrimary"<<endl;
    spout<<"      implicit none"<<endl;
    spout<<"c"<<endl;
    spout<<"      double precision  cspeed"<<endl;
    spout<<"      parameter         (cspeed=299792458.d0)"<<endl;
    spout<<"c"<<endl;
    spout<<"c     Version numbers -----------------------"<<endl;
    spout<<"      integer           mvnew, mvold"<<endl;
    spout<<"      parameter         (mvnew="<<version<<")"<<endl;
    spout<<"c"<<endl;
    spout<<"c     Variables for speistart call  ---------"<<endl;
    spout<<"c"<<endl;
    spout<<"      integer           shower_number"<<endl;
    spout<<"      double precision  primary_energy"<<endl;
    spout<<"      double precision  default_injection_position(3)"<<endl;
    spout<<"      double precision  injection_depth, ground_depth"<<endl;
    spout<<"      double precision  ground_altitude, d_ground_inj"<<endl;
    spout<<"      double precision  shower_axis(3)"<<endl;
    spout<<"c"<<endl;
    spout<<"c     Global Return code --------------------------"<<endl;
    spout<<"      integer           rc"<<endl;
    spout<<"c"<<endl;
    spout<<"c     general variables for spinjpoint --------"<<endl;
    spout<<"c"<<endl;
    spout<<"      integer           injcsys"<<endl;
    spout<<"      parameter         (injcsys=0) !Uses Aires CS"<<endl;
    if (InvertPhi)
      {
	spout<<"      double precision  x0,y0,z0    !coord. of decay"<<endl;
	spout<<"      integer           tsw"<<endl;
	spout<<"      parameter         (tsw=1) !switch: t0beta=beta"<<endl;
	spout<<"      double precision  t0beta"<<endl;
	spout<<"      parameter         (t0beta=1.d0)"<<endl;
      }
    else
      {
	spout<<"      double precision  x0,y0,z0,z0c    !coord. of decay"<<endl;
	spout<<"      integer           tsw"<<endl;
	spout<<"      parameter         (tsw=0) !switch: t0beta=t in ns"<<endl;
	spout<<"      double precision  t0beta"<<endl;
	
      }
    spout<<"      integer           jrc        !return code"<<endl;
    spout<<"c"<<endl;
    spout<<"c     general variables for spadddp0 --------"<<endl;
    spout<<"c"<<endl;
    spout<<"      integer           pcode      !pcle code"<<endl;
    spout<<"      double precision  penergy    !pcle energy"<<endl;
    spout<<"      integer           csys"<<endl;
    spout<<"      parameter         (csys=0)   !Uses AIRES CS"<<endl;
    spout<<"      double precision  ux, uy, uz !pcle direction"<<endl;
    spout<<"      double precision  pwt        !particle weight"<<endl;
    spout<<"      parameter         (pwt=1.d0)"<<endl;
    spout<<"      integer           irc        !return code"<<endl;
    spout<<"c"<<endl;
    spout<<"c"<<endl;

    ////////////////////////////////////////////////////////////////// 
      
    //Call speistart (all output variables)

    spout<<"c     ----- BEGIN BLOCK -----------------------"<<endl;
    spout<<"c"<<endl;
    spout<<"      call speistart(shower_number, primary_energy,"<<endl;
    spout<<"     +               default_injection_position, injection_depth,"<<endl;
    spout<<"     +               ground_altitude, ground_depth,"<<endl;
    spout<<"     +               d_ground_inj, shower_axis)"<<endl;
    spout<<"c"<<endl;
        
    //Call spinjpoint (uses already calculated Xinj, Yinj, Zinj)
    spout<<"c     set global return code to 0"<<endl;
    spout<<"      rc=0"<<endl;
    spout<<"c"<<endl;
    spout<<"c     decay point coordinates in AIRES coordinate system"<<endl;
    spout<<"      x0="<<Xinj<<endl;
    spout<<"      y0="<<Yinj<<endl;
    spout<<"      z0="<<Zinj<<endl;
    if(!InvertPhi)
    {
      spout<<"      z0c="<<Zinj-GroundAltitude<<endl;
      spout<<"      t0beta=sqrt(x0*x0+y0*y0+z0c*z0c)/299792458.d-9"<<endl;
    }
    spout<<"      call spinjpoint(injcsys, x0, y0, z0, tsw, t0beta, jrc)"<<endl;
    spout<<"c"<<endl;
    spout<<"c     test return code and change global return code if needed"<<endl;
    spout<<"      if(jrc .ne. 0) then"<<endl;
    spout<<"          rc=jrc"<<endl;
    spout<<"          print*,\"spinjpoint retuned non zero exit code! jrc=\",jrc"<<endl;
    spout<<"      end if"<<endl;
    spout<<"c"<<endl;
    

    //Callls to spaddp0. One for each Decay product
    //Uses the AIRES coordinate system.
    //To transfor it from the decay CS (z in line with primary Tau momentum)
    //to the Aires CS we make two rotations: Phi around the z axis and 
    //theta around the new y axis
    

    //Energy is corrected by the already calculated EnergyScaleFactor and
    // transformed from eV to GeV
    
    spout<<"c   Start calls to spaddp0 ---------------------------------"<<endl;
    spout<<"c"<<endl;
    
    //secondary particle loop
    int n=pArray.size();
    Vector3D UDecay,UAires;
    //test
    Vector3D UDecayN,UAiresN,UNAires;
    
    for (int i=0;i<n;i++)
    {
	
	UDecay.SetCart(pArray[i].px,pArray[i].py,pArray[i].pz);
	//UDecay.MakeUnit();
	UAires=RotateBack(Theta,Phi,UDecay); //normalized then rotated
	

	spout<<"c     PARTICLE "<<i+1<<"-------------------"<<endl;
	spout<<"      pcode="<<pArray[i].Code<<endl;
	spout<<"      ux="<<UAires.x<<endl;
	spout<<"      uy="<<UAires.y<<endl;
	spout<<"      uz="<<UAires.z<<endl; 
	
	if(Verbose)
	{
	    cout<<endl<<"Particle "<<i+1<<":"<<endl;
	    cout<<"UDecay:"<<endl;
	    UDecay.Print();
	    cout<<"Rotation: Theta="<<Theta*r2d<<" deg  Phi="<<Phi*r2d<<" deg"<<endl;
	    cout<<"UAires:"<<endl;
	    UAires.Print();

	}
	
	//energy scaled by EnergyScaleFactor and transformed from eV to GeV
	spout<<"      penergy="<<pArray[i].E*EnergyScaleFactor/1.e9<<" !GeV"<<endl;
	spout<<"c"<<endl;
	spout<<"      call spaddp0(pcode,penergy,csys,ux,uy,uz,pwt,irc)"<<endl;
	spout<<"c"<<endl;
	spout<<"c     test return code and change global one if needed"<<endl;
	spout<<"c"<<endl;
	// test return code and change global return code if applicable
	spout<<"      if(irc .ne. 0) then"<<endl;
        spout<<"             rc=irc"<<endl;
	spout<<"             print*,\"spaddp0 retuned non zero exit code! irc=\",irc"<<endl;
	spout<<"      end if"<<endl;
	spout<<"c"<<endl;
    } //end of secondary particle loop

    spout<<"c   End of calls to spaddp0 -------------------------------"<<endl;
    spout<<"c"<<endl;
    
    spout<<"      if(rc .ne. 0) then"<<endl;
    spout<<"             print*,\"Something went wrong!!!\""<<endl;
    spout<<"             print*,\"global return code is non zero! rc=\",rc"<<endl;
    spout<<"      end if"<<endl;
    spout<<"c"<<endl;
    spout<<"c      Setting macro version -----------"<<endl;
    spout<< "      call speimv(mvnew, mvold)"<<endl;
    spout<<"c"<<endl;
    spout<<"c      Calling speiend with global return code-----------"<<endl;
    spout<< "      call speiend(rc)"<<endl;
    spout<<"c"<<endl;
    spout<< "      end"<<endl;
    

    cout<<endl<<"Done!"<<endl;
    SpecialParticleFileWritten=true;
    //close Special particle file
    spout.close();

    //compile particle file
    string compileString = "gfortran -fPIC " + SpecialParticleFileName + " -L/home/hin/harmscho/aires/2-8-4a/lib -lAires -o " + SpecialParticleExecutableFileName;
    cout << "Compiling special particle:" <<  compileString << endl;
     system(compileString.c_str());    
}



void NuSimPreparator::CreateAiresInpFile()
{
    //test if Special Particle file was already written
    if(!SpecialParticleFileWritten)
    {
	cout<<endl<<"(EE) NuSimPreparator::CreateAiresInpFile error:"<<endl;
	cout<<"     Special Particle file was not written yet!"<<endl;
	cout<<"     Before writing the .inp file you must first create the Special Particle file!"<<endl;
	cout<<"     Exiting...."<<endl;
	exit(-1);
    }
    //test if array is aet
    if(!ArrayIsSet)
    {
	cout<<endl<<"(EE) NuSimPreparator::CreateAiresInpFile error:"<<endl;
	cout<<"     Antenna array was not set yet!"<<endl;
	cout<<"     Before writing the .inp file you must first set the antenna array!"<<endl;
	cout<<"     Exiting...."<<endl;
	//exit(-1);
    }

    AiresInpFileName=TaskName+".inp";
    cout<<"Aires .inp file name is set to: "<<AiresInpFileName<<endl;

    //test if output file exists and give options (interactive) ////////
    ////////////////////////////////////////////////////////////////////
    if( FileExists(AiresInpFileName) )
    {
	cout<<endl<<"File Exists! Do you want to overwrite it? y/n"<<endl;
	char answer;
	cin>>answer;
	if (answer=='n' || answer=='N')
	{
	    cout<<"Ok! Exiting..."<<endl;
	    exit(-1);
	}
	cout<<endl<<"Will overwrite file..."<<SpecialParticleFileName<<endl;
    }
    ////////////////////////////////////////////////////////////////////
    //////////////////// End test if file exists ///////////////////////

    

    cout<<endl<<"Writting Aires .inp file...."<<endl;


    //Opening output file
    ofstream airesout(AiresInpFileName.c_str());



    //// Write Aires .inp file /////////////////////
    ////////////////////////////////////////////////

    airesout<<"#################################################"<<endl;
    airesout<<"####  File created by NuSimPreparator v"<<VERSIONSP<<"  ####"<<endl;
    airesout<<"###############################################"<<endl<<endl;

    
    airesout<<"TaskName "<<TaskName<<endl;
    airesout<<endl;
    airesout<<endl;
    airesout<<"AddSpecialParticle jet ./"<<SpecialParticleExecutableFileName<<endl;
    airesout<<endl;
    airesout<<"PrimaryParticle jet"<<endl;
    airesout<<"PrimaryEnergy "<<DesiredTauEnergy<<" eV"<<endl; 
    airesout<<"PrimaryZenAngle "<<AiresThetaDeg<<" deg"<<endl;
    airesout<<"PrimaryAzimAngle "<<AiresPhiDeg<<" deg Magnetic"<<endl;
    airesout<<endl;
    airesout<<"GeomagneticField "<<GeomagneticFielduT<< " uT "<<GeomagneticInclinationDeg<<" deg 0.0 deg"<<endl;
    airesout<<endl;
    airesout<<"GroundAltitude "<<GroundAltitude<<" m"<<endl;
    airesout<<endl;
    airesout<<"TotalShowers 1"<<endl;
    airesout<<"RunsPerProcess Infinite"<<endl;
    airesout<<"RandomSeed "<<RandomSeed<<endl;
    airesout<<endl;
    airesout<<"ElectronCutEnergy 80 keV"<<endl; 
    airesout<<"ElectronRoughCut  80 keV"<<endl; 
    airesout<<"GammaCutEnergy 80 keV"<<endl; 
    airesout<<"GammaRoughCut 80 keV"<<endl; 
    airesout<<endl;
    airesout<<"InjectionAltitude 100 km"<<endl; 
    airesout<<endl;
    airesout<<"ForceModelName SIBYLL "<<endl;   
    airesout<<"ThinningEnergy "<<ThinningEnergy<<" Relative"<<endl; 
    airesout<<"ThinningWFactor 0.06"<<endl;
    airesout<<endl;
    airesout<<"#Output Handling"<<endl; 
    airesout<<"ObservingLevels 510"<<endl; 
    airesout<<"PerShowerData Full"<<endl; 
    airesout<<"SaveNotInFile lgtpcles All"<<endl; 
    airesout<<"SaveNotInFile grdpcles All"<<endl; 
    airesout<<"ExportPerShower"<<endl; 
    airesout<<endl;
    airesout<<"#ZHAireS"<<endl; 
    airesout<<endl;
    airesout<<"ZHAireS On"<<endl; 
    airesout<<"FresnelTime On"<<endl;
    airesout<<"TimeDomainBin "<<TimeDomainBin<<" ns"<<endl;
    airesout<<endl;
    airesout<<endl;
    //Antennas
    int n=Antennas.size();
    airesout<<"# There is a total of "<<n<<" antennas for this whole event."<<endl;
    // Antenna loop
    for(int i=0;i<n;i++) airesout<<"AddAntenna  "<<Antennas[i].x<<"   "<<Antennas[i].y<<"   "<<Antennas[i].z<<endl;    

    //// Finished writting .inp file /////////////////////
    //////////////////////////////////////////////////////
    

    cout<<"Done!"<<endl;
    AiresInpFileWritten=true;

}



void NuSimPreparator::CreateAntennaArray(double d, double length, double width)
{
    if(!ShowerGeometryIsSet || !DecayIsSet)
    {
	cout<<endl<<"(EE) NuSimPreparator::CreateAntennaArray error:"<<endl;
	cout<<"     Shower geometry or decay is not set yet! Exiting...."<<endl;
	exit(-1);	
    }
    
    //calculate vXmax 
    //Xmax is on the axis at a distance DXmax from the decay point
    vXmax=vDecay+(vAxis*DXmax);

    if(Verbose) 
    {
	cout<<endl<<"DXmax="<<DXmax<<endl;
	cout<<"VDecay:"<<endl;
	vDecay.Print();
	cout<<"VAxis:"<<endl;
	vAxis.Print();
	cout<<"VXmax:"<<endl;
	vXmax.Print();
    }

    //calculate Cherenkov angle at Xmax
    double altkm=(vXmax.z+GroundAltitude)*1.e-3;  //altitude of Xmax in km
    double Rh=R0*exp(-kr*altkm);
    double nh=1.+1.e-6*Rh;
    double costetc=1./nh;
    double CherAngle=acos(costetc);
    double CherAngleDeg=CherAngle*r2d;
    if(Verbose) cout<<endl<<"Cherenkov angle at Xmax is "<<CherAngleDeg<<" deg."<<endl;
    

    //calculate minPsi and MaxPsi (based on MaxAngularDifferenceDeg)
    //uses either the default MaxAngularDifferenceDeg or a previous one set by the user
    MaxAngleIn=MaxAngleInDeg*d2r;
    MaxAngleOut=MaxAngleOutDeg*d2r;

    MinPsi=CherAngle-MaxAngleIn;
    MaxPsi=CherAngle+MaxAngleOut;

    if(Verbose) cout<<endl<<"Maximum angle in: "<<MaxAngleInDeg<<" Maximum angle out: "<<MaxAngleOutDeg<<endl;
    if(Verbose) cout<<"Maximum Psi: "<<MaxPsi*r2d<<"  Minimum Psi: "<<MinPsi*r2d<<endl;

    //Vector from Xmax along the maximum Psi direction in a vertical plane.
    //This represents the direction to the closest "impact point" of the signal
    //on the ground (cone of opening MaxPsi from Xmax).
    Vector3D vMaxPsiProp; 

    //set as unit vector first
    //Note that if Theta+MaxPsi<90 deg, the propagation does not reach 
    //the ground for this particular MaxPsi 
    if( (Theta+MaxPsi)< pi/2. )
    {
	cout<<endl<<"(WW) NuSimPreparator::CreateAntennaArray warning:"<<endl;
	cout<<"     MaxPsi is too small (It's set to "<<MaxPsi*r2d<<" deg). Since the zenithal angle is set to "<<ThetaDeg<<" deg,"<<endl<<"     the emission cone does not reach the ground!"<<endl;
	cout<<"     Try increasing MaxAngularDifference to at least "<<90-ThetaDeg-CherAngleDeg<<"deg "<<endl<<"     or decrease zenithal angle to at least "<<90-MaxPsi*r2d<<" deg..."<<endl;
	cout<<"     Did not create any array! Returning...."<<endl;
	return;
    }
    vMaxPsiProp.SetSpher(1.,Theta+MaxPsi,Phi);
    

    //calculate the distance from Xmax to ground along propagation vMaxPsiProp
    double L=-(vXmax.z/vMaxPsiProp.z);
    vMaxPsiProp*=L;   //now vMaxPsiProp goes from Xmax to grond

    Vector3D vImpactPoint=vXmax+vMaxPsiProp;
    
    //calculate vector vImpactPoint from core to impact point in coreCS
    //This should always be horizontal!!!
    vImpactPoint=vXmax+vMaxPsiProp;
    if(Verbose)
    {
	cout<<endl<<"vImpactPoint:"<<endl;
	vImpactPoint.Print();
    }

    
    //Will make the AntennaArray core so that the impact point is at the 
    //beginning of the array plus a distance d (antenna distance)
    double corex=-(vImpactPoint.r + length/2. -d );
     
    

    //Set array dimensions of Array (AntennaArray)
    //this will set the array with length along the NS direction
    Array.SetDimensions(d,length,width);
    //Set core in ArrayCS
    Array.SetCore(corex,0,0);
    

    //rotate the array so that it is aligned with the ground projection of the
    //actual axis along its length (rotate by theta=0 and phi="axis phi"
    Array.RotateArrayCoreCS(0.,-vImpactPoint.phi);

    //SetXmax in CoreCS
    Array.SetXmax(vXmax.x,vXmax.y,vXmax.z);

    //calculate Psi 
    Array.CalculatePsi();

    
    if(FilterArray)  //filter array in the range MinPsi-MaxPsi
    {
	Array.SetMinPsi(MinPsi);
	Array.SetMaxPsi(MaxPsi);
	if(Verbose) cout<<"Min/Max Psi for filtering:"<<Array.GetMinPsi()*r2d<<" "<<Array.GetMaxPsi()*r2d<<endl;

	Array.FilterArray();
	
	//now Array.aArrayFiltered contains the filtered antennas!
	Antennas=Array.aArrayFiltered;
	//Write filtered array file (for checking... not really used)
	Array.WriteArrayFilteredFile();
    }
    
    else 
    {
	Antennas=Array.aArrayCoreCS;   //do not filter
	Array.WriteArrayCoreCSFile();
    }
    
    ArrayIsSet=true;
    
    
}



//perpendicular array

void NuSimPreparator::CreatePerpendicularAntennaArray(double d, double length, double width, double dxmax)
{
    if(!ShowerGeometryIsSet || !DecayIsSet)
    {
	cout<<endl<<"(EE) NuSimPreparator::CreateAntennaArray error:"<<endl;
	cout<<"     Shower geometry or decay is not set yet! Exiting...."<<endl;
	exit(-1);	
    }
    
    //calculate vXmax 
    //Xmax is on the axis at a distance DXmax from the decay point
    vXmax=vDecay+(vAxis*DXmax);

    if(Verbose) 
    {
	cout<<endl<<"DXmax="<<DXmax<<endl;
	cout<<"VDecay:"<<endl;
	vDecay.Print();
	cout<<"VAxis:"<<endl;
	vAxis.Print();
	cout<<"VXmax:"<<endl;
	vXmax.Print();
    }

    //calculate Cherenkov angle at Xmax
    double altkm=(vXmax.z+GroundAltitude)*1.e-3;  //altitude of Xmax in km
    double Rh=R0*exp(-kr*altkm);
    double nh=1.+1.e-6*Rh;
    double costetc=1./nh;
    double CherAngle=acos(costetc);
    double CherAngleDeg=CherAngle*r2d;
    if(Verbose) cout<<endl<<"Cherenkov angle at Xmax is "<<CherAngleDeg<<" deg."<<endl;
    

    //calculate minPsi and MaxPsi (based on MaxAngularDifferenceDeg)
    //uses either the default MaxAngularDifferenceDeg or a previous one set by the user
    MaxAngleIn=MaxAngleInDeg*d2r;
    MaxAngleOut=MaxAngleOutDeg*d2r;

    MinPsi=CherAngle-MaxAngleIn;
    MaxPsi=CherAngle+MaxAngleOut;

    if(Verbose) cout<<endl<<"Maximum angle in: "<<MaxAngleInDeg<<" Maximum angle out: "<<MaxAngleOutDeg<<endl;
    if(Verbose) cout<<"Maximum Psi: "<<MaxPsi*r2d<<"  Minimum Psi: "<<MinPsi*r2d<<endl;

    // //Vector from Xmax along the maximum Psi direction in a vertical plane.
    // //This represents the direction to the closest "impact point" of the signal
    // //on the ground (cone of opening MaxPsi from Xmax).
    // Vector3D vMaxPsiProp; 

    // //set as unit vector first
    // //Note that if Theta+MaxPsi<90 deg, the propagation does not reach 
    // //the ground for this particular MaxPsi 
    // if( (Theta+MaxPsi)< pi/2. )
    // {
    // 	cout<<endl<<"(WW) NuSimPreparator::CreateAntennaArray warning:"<<endl;
    // 	cout<<"     MaxPsi is too small (It's set to "<<MaxPsi*r2d<<" deg). Since the zenithal angle is set to "<<ThetaDeg<<" deg,"<<endl<<"     the emission cone does not reach the ground!"<<endl;
    // 	cout<<"     Try increasing MaxAngularDifference to at least "<<90-ThetaDeg-CherAngleDeg<<"deg "<<endl<<"     or decrease zenithal angle to at least "<<90-MaxPsi*r2d<<" deg..."<<endl;
    // 	cout<<"     Did not create any array! Returning...."<<endl;
    // 	return;
    // }
    // vMaxPsiProp.SetSpher(1.,Theta+MaxPsi,Phi);
    

    // //calculate the distance from Xmax to ground along propagation vMaxPsiProp
    // double L=-(vXmax.z/vMaxPsiProp.z);
    // vMaxPsiProp*=L;   //now vMaxPsiProp goes from Xmax to grond

    // Vector3D vImpactPoint=vXmax+vMaxPsiProp;
    
    // //calculate vector vImpactPoint from core to impact point in coreCS
    // //This should always be horizontal!!!
    // vImpactPoint=vXmax+vMaxPsiProp;
    // if(Verbose)
    // {
    // 	cout<<endl<<"vImpactPoint:"<<endl;
    // 	vImpactPoint.Print();
    // }

    
    // //Will make the AntennaArray core so that the impact point is at the 
    // //beginning of the array plus a distance d (antenna distance)
    // double corex=-(vImpactPoint.r + length/2. -d );
     
    

    //Set array dimensions of Array (AntennaArray)
    //this will set the array with length along the NS direction
    Array.SetDimensions(d,length,width);
    //Set core in ArrayCS
    //Array.SetCore(corex,0,0);
    Array.SetCore(0,0,0);

    //SetXmax in CoreCS
    Array.SetXmax(vXmax.x,vXmax.y,vXmax.z);

    //Create perpendicular array
    Array.CreatePerpendicularArrayCoreCS(dxmax);

    //calculate Psi 
    Array.CalculatePsi();

    
    if(FilterArray)  //filter array in the range MinPsi-MaxPsi
    {
	Array.SetMinPsi(MinPsi);
	Array.SetMaxPsi(MaxPsi);
	if(Verbose) cout<<"Min/Max Psi for filtering:"<<Array.GetMinPsi()*r2d<<" "<<Array.GetMaxPsi()*r2d<<endl;

	Array.FilterArray();
	
	//now Array.aArrayFiltered contains the filtered antennas!
	Antennas=Array.aArrayFiltered;
	//Write filtered array file (for checking... not really used)
	Array.WriteArrayFilteredFile();
    }
    
    else 
    {
	Antennas=Array.aArrayCoreCS;   //do not filter
	Array.WriteArrayCoreCSFile();
    }
    
    ArrayIsSet=true;
    
    
}
//end peprpendicular array

//Perpendicular Array centered at ANITA altitude
void NuSimPreparator::CreateAnitaPerpendicularAntennaArray(double d, double length, double width, double anitaaltitude)
{
  if(!ShowerGeometryIsSet || !DecayIsSet)
    {
	cout<<endl<<"(EE) NuSimPreparator::CreateAnitaPerpendicularAntennaArray error:"<<endl;
	cout<<"     Shower geometry or decay is not set yet! Exiting...."<<endl;
	exit(-1);	
    }

  double AnitaHeight=anitaaltitude-GroundAltitude;  //ANITA height above ground

  //calculate distance from Xmax to point on axis at Anita Height

  double DAnt; //Distance from core to center of array
  double RAnt; //Projection along plane perpendicular to z axis (at ground altitude)
  double dxmax; //distance from Xmax to center of array
  
  double Rt=6.371e6;      //earth radius in m
  double Rtc=Rt+GroundAltitude; //corrected radius


  
  //case 1: Plane Earth /////////////////////
    if(UsePlaneEarth)
      {
	if(Verbose) cout<<endl<<"Using plane Earth approximation to calculate Anita array..."<<endl;
	DAnt=AnitaHeight/CosTet;
      }

    
    //case 2: Curved Earth /////////////////////
    //Eq: (Rtc+AnitaHeight)**2 = (Rtc+z)**2 + Rant**2, where
    //    Rant=D*sin(theta)  and z=D*cos(theta)
    else
      {
	if(Verbose) cout<<endl<<"Using  full curved Earth calculation of Anita perpendicular array..."<<endl;
	double A=1;
	double B=2*Rtc*CosTet;
	double C=-(AnitaHeight*AnitaHeight)-2*Rtc*AnitaHeight;
	double Delta=B*B-4*A*C;
	double D=(-B+sqrt(Delta))/2*A;

	RAnt=D*SinTet;
	DAnt=D;
      }

    
    //calculate vXmax 
    //Xmax is on the axis at a distance DXmax from the decay point
    vXmax=vDecay+(vAxis*DXmax);
    //calculate dxmax (distance from Xmax to center of array)
    dxmax=DAnt-vXmax.r; 

    int dummy;
    if(Verbose) 
    {
	cout<<endl<<"DXmax="<<DXmax<<endl;
	cout<<"VDecay:"<<endl;
	vDecay.Print();
	cout<<"VAxis:"<<endl;
	vAxis.Print();
	cout<<"VXmax:"<<endl;
	vXmax.Print();
	cout<<"DAnt="<<DAnt<<" dxmax="<<dxmax<<endl;
    }

    //create array
    CreatePerpendicularAntennaArray(d, length, width, dxmax);
    
}
//END Perpendicular Array centered at ANITA altitude


////////////////////////////////////////////////////////////////
//Dump and message methods   /////////////////////////////////// 
////////////////////////////////////////////////////////////////


void NuSimPreparator::DumpShowerGeometry()
{
    cout<<"______________________________________________"<<endl;
    if(!ShowerGeometryIsSet) 
    {
	cout<<"Shower Geometry is not set yet!"<<endl;
	return;
    }
    cout<<endl<<"Shower Geometry:"<<endl;
    cout<<"Aires (downgoing):   AiresThetaDeg="<<AiresThetaDeg<<"  AiresPhiDeg="<<AiresPhiDeg<<endl;
    cout<<"\"Real\"  (downgoing):   ThetaDeg="<<ThetaDeg<<"       PhiDeg="<<PhiDeg<<endl;
    cout<<"GroundAltitude="<<GroundAltitude<<" m"<<endl;
    cout<<"Decayheight="<<DecayHeight<<" m  Decay Distance="<<DecayDistance<<" m  DecayGroundDistance="<<DecayGroundDistance<<" m"<<endl;
    cout<<"Injection Point coordinates in Aires system (m):"<<endl<<"Xinj="<<Xinj<<"  Yinj="<<Yinj<<"  Zinj="<<Zinj<<endl;
    if(UsePlaneEarth) cout<<"Used plane Earth approximation for injection point calculation!"<<endl;
    cout<<"______________________________________________"<<endl;
    return;

}
void NuSimPreparator::DumpDecayInput()
{
    cout<<"______________________________________________"<<endl;
    if(!CheckDecayInput()) 
    {
	cout<<endl<<"Decay input file and/or Decay number to use  not set yet!"<<endl;
	return;
    }
    cout<<endl<<"Decay Input:"<<endl;
    cout<<"Decay input file:    "<<DecayInputFile<<endl;
    cout<<"decay number to use: "<<DecayN<<endl;
    cout<<"______________________________________________"<<endl;
    return;
}

void NuSimPreparator::DumpParticleArray()
{
    cout<<"______________________________________________"<<endl;
    cout<<endl<<"Decay Info:"<<endl;
    if(!DecayIsSet)
    {
	cout<<endl<<"Decay is not set yet!"<<endl;
	return;
    }
    
    cout<<"TauEnergy:"<<TauEnergy<< "  ShowerEnergy:"<<ShowerEnergy<<endl;
    cout<<"Number of secondaries: "<<pArray.size()<<endl;

    for(int i=0;i<pArray.size();i++)	cout<<"particle "<<i<<": code: "<<pArray[i].Code<<"    p:("<<pArray[i].px<<","<<pArray[i].py<<","<<pArray[i].pz<<")    E:"<<pArray[i].E<<endl;
    cout<<"______________________________________________"<<endl;
}


void NuSimPreparator::Hello()
    {
	cout<<"********************************"<<endl;
	cout<<"********************************"<<endl;
	cout<<"**   NuSimPreparator v"<<VERSIONSP<<"     **"<<endl;
	cout<<"********************************"<<endl;
	cout<<"********************************"<<endl;
    }

/////////////////////////////////////////////////////////////////////
