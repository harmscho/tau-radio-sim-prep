#include "AntennaArray.h"

int main()
{
    double d=50;
    
    //double length=500;
    //double width=500;
    
    double length=50000;
    double width=300;

    double corex=-60000;
    //double corex=0;
    double corey=0;
    double corez=0;

     double xmaxx=10000;
     double xmaxy=0;
     double xmaxz=100;
    
     //double xmaxx=100;
     //double xmaxy=0;
     //double xmaxz=10;

    double dxmax=5000;
    

    double theta=45., phi=0;

    ////////////////////
    AntennaArray array;
    
    array.SetVerboseOff();

    array.SetDimensions(d, length, width);
    array.SetCore(corex,corey,corez);
    array.SetXmax(xmaxx,xmaxy,xmaxz);
    
    //array.SetShowerIsUpgoingInCoreCS(false);

    //array.RotateArrayCoreCS(theta,phi);
    //array.TranslateArrayCoreCS(0,0,50);
    
    //array.CreatePerpendicularArrayCoreCS(dxmax);


    array.CalculatePsi();
    /////////////////////

    // AntennaArray array(d, length, width);

    // array.SetCore(-250,0,0);

    /////////////////////
    
    
    // AntennaArray array(d, length, width, corex,corey,corez);


    /////////////////////

    array.SetMinPsiDeg(0.8);
    array.SetMaxPsiDeg(1.2);
    array.FilterArray();
    

    array.WriteArrayFile();
    array.WriteArrayCoreCSFile();
    array.WriteArrayFilteredFile();


}
    
