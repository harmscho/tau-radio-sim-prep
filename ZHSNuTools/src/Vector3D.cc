/* This is a new version of the Vector struct in Toymodel.h */

#include "Vector3D.h"


//Constructors
////////////////////////
    
//Default constructor
Vector3D::Vector3D():x(0),y(0),z(0),r(0),theta(0),phi(0){}
	
//Set either cartesian or spherical coordinates (IsCartesian bool control)
Vector3D::Vector3D(double c1, double c2, double c3, bool IsCartesian)
{
    if(IsCartesian) SetCart(c1,c2,c3);
    else SetSpher(c1,c2,c3);
}
    

//Set Methods ///////////////////
/////////////////////////////////////////////////////////

//Set vector using cartesian coordinates 
//Also automatically sets spherical coordinates from cartesian input
void Vector3D::SetCart(double xin,double yin,double zin)
{
    x=xin;
    y=yin;
    z=zin;
    double rho= sqrt(x*x+y*y);
    r=sqrt(x*x+y*y+z*z);
    if(rho==0)
    {
	if(z>=0) theta=0;
	else theta=acos(-1);
	phi=0;
    }
    else
    {
	theta=atan2(rho,z);
	phi=atan2(y,x);
	if(phi<0.0) phi+=2.*acos(-1.); //if phi is negative add 2pi
    }
}

//Set vector using spherical coordinates 
//Also automatically sets cartesian coordinates from spherical input
void Vector3D::SetSpher(double rin,double thetain,double phiin)
{
    r=rin;
    theta=thetain;
    phi=phiin;
    x=r*sin(theta)*cos(phi);
    y=r*sin(theta)*sin(phi);
    z=r*cos(theta);

    //round "close" zeroes. Depends on r
    if( fabs(x)<(r*1.e-15) ) x=0;
    if( fabs(y)<(r*1.e-15) ) y=0;
    if( fabs(z)<(r*1.e-15) ) z=0;
}


//Transforms and operations ///////////////
///////////////////////////////////////////////////

//Make the vector a unit vector
    void Vector3D::MakeUnit() {SetSpher(1.0,theta,phi);}

    //Multiplies this vector by a scalar 
    void Vector3D::Multiply(double k) {SetCart(x*=k,y*=k,z*=k);}

    //Divides this vector by a scalar
    void Vector3D::Divide(double k) {SetCart(x/=k,y/=k,z/=k);}

    //Add another vector to this one
    void Vector3D::Add(Vector3D v) {SetCart(x+v.x,y+v.y,z+v.z);}

    //Subtract another vector from this one
    void Vector3D::Subtract(Vector3D v) {SetCart(x-v.x,y-v.y,z-v.z);}

void Vector3D::Print(){
    double r2d=180./acos(-1);
    cout<<"x="<<x<<endl;
    cout<<"y="<<y<<endl;
    cout<<"z="<<z<<endl;
    cout<<"r="<<r<<endl;
    cout<<"theta="<<theta*r2d<<" deg"<<endl;
    cout<<"phi="<<phi*r2d<<" deg"<<endl;
}

Vector3D Vector3D::operator *(double k)
{
    Vector3D v(k*x,k*y,k*z,true);
    return v;
}

Vector3D Vector3D::operator /(double k)
{
    Vector3D v(x/k,y/k,z/k,true);
    return v;
}

Vector3D Vector3D::operator +(Vector3D v)
{
    Vector3D vsum(x+v.x,y+v.y,z+v.z,true);
    return vsum;
}

Vector3D Vector3D::operator -(Vector3D v)
{
    Vector3D vsub(x-v.x,y-v.y,z-v.z,true);
    return vsub;
}

Vector3D& Vector3D::operator *=(double k)
{
    SetCart(k*x,k*y,k*z);
    return *this;
}
Vector3D& Vector3D::operator /=(double k)
{
    SetCart(x/k,y/k,z/k);
    return *this;
}

Vector3D& Vector3D::operator +=(Vector3D v)
{
    SetCart(x+v.x,y+v.y,z+v.z);
    return *this;
}

Vector3D& Vector3D::operator -=(Vector3D v)
{
    SetCart(x-v.x,y-v.y,z-v.z);
    return *this;
}

//NOTE: Replaced this overloaded operators by class member ones
///////////////////////////////////////////////////////////////
// //v=v1+v2
// Vector3D operator+(Vector3D v1,Vector3D v2)
// {
//     Vector3D v(v1.x+v2.x, v1.y+v2.y, v1.z+v2.z,true);
//     return v;
// }

// //v=v1-v2
// Vector3D operator-(Vector3D v1,Vector3D v2)
// {
//     Vector3D v(v1.x-v2.x, v1.y-v2.y, v1.z-v2.z,true);
//     return v;
// }

// //v=k*vin , k is scalar vin is vector
// Vector3D operator  *(double k,Vector3D vin)
// {
//     Vector3D v(k*vin.x,k*vin.y,k*vin.z,true);
//     return v;
// }
///////////////////////////////////////////////////////////////


//Dot Product
double DotProd(Vector3D v1,Vector3D v2) { return v1.x*v2.x+v1.y*v2.y+v1.z*v2.z;}

//Cross Product
Vector3D CrossProd(Vector3D v1,Vector3D v2)
{
    double x,y,z;
    
    x= ((v1.y*v2.z)-(v1.z*v2.y));
    y =((v1.z*v2.x)-(v1.x*v2.z));
    z =((v1.x*v2.y)-(v1.y*v2.x));
    Vector3D v(x,y,z,true);
    return v;
}

//Rotate
Vector3D Rotate(double Theta,double Phi, Vector3D vIn)
{
  double A[3][3];   // TRANSFORMATION MATRIX
  //C     1ST Line
  A[0][0]=cos(Theta)*cos(Phi);
  A[0][1]=cos(Theta)*sin(Phi);
  A[0][2]=-sin(Theta);
  //C     2ND Line
  A[1][0]=-sin(Phi);
  A[1][1]=cos(Phi);
  A[1][2]=0.0;
  //C     3RD Line
  A[2][0]=sin(Theta)*cos(Phi);
  A[2][1]=sin(Theta)*sin(Phi);
  A[2][2]=cos(Theta);

  //C     TRANSFORMATION [XYZ]v=A*[XYZ]vIn

  double x,y,z;
  x=A[0][0]*vIn.x+A[0][1]*vIn.y+A[0][2]*vIn.z;
  y=A[1][0]*vIn.x+A[1][1]*vIn.y+A[1][2]*vIn.z;
  z=A[2][0]*vIn.x+A[2][1]*vIn.y+A[2][2]*vIn.z;
  Vector3D v(x,y,z,true);
  return v;
}

//Rotate Back (inverse rotation)
Vector3D RotateBack(double Theta,double Phi, Vector3D vIn)
{
  double A[3][3];   // TRANSFORMATION MATRIX
  //C     1ST COLUMN
  A[0][0]=cos(Theta)*cos(Phi);
  A[1][0]=cos(Theta)*sin(Phi);
  A[2][0]=-sin(Theta);
  //C     2ND COLUMN
  A[0][1]=-sin(Phi);
  A[1][1]=cos(Phi);
  A[2][1]=0.0;
  //C     3RD COLUMN
  A[0][2]=sin(Theta)*cos(Phi);
  A[1][2]=sin(Theta)*sin(Phi);
  A[2][2]=cos(Theta);

  //C     TRANSFORMATION [XYZ]v=A*[XYZ]vIn

  double x,y,z;
  x=A[0][0]*vIn.x+A[0][1]*vIn.y+A[0][2]*vIn.z;
  y=A[1][0]*vIn.x+A[1][1]*vIn.y+A[1][2]*vIn.z;
  z=A[2][0]*vIn.x+A[2][1]*vIn.y+A[2][2]*vIn.z;
  Vector3D v(x,y,z,true);
  return v;
}


//returns angle between vec1 and vec2
double VecAngle(Vector3D vec1,Vector3D vec2)
{
  double DP; //dot product of the two vectors
  double Mod1,Mod2; //length of the two vectors
  double ModMult; //product of lengths
  double CosAngle; //cosine of the angle between vectors

  //first we calculate the length of the vectors
  Mod1=vec1.r;
  Mod2=vec2.r;
  //then their dot product
  DP=DotProd(vec1,vec2);
  //     WE CALCULATE THE COSINE OF THE ANGLE BETWEEN THEM
  //     TESTING THE DENOMINATOR FIRST

  ModMult=Mod1*Mod2;
  if(ModMult==0.0) 
    {
      ModMult=1.e-199;
    }
  CosAngle=DP/ModMult;
  //     AND CHECK FOR PRECISION ERRORS BEFORE USING DACOS
  if(CosAngle < -1.0) CosAngle=-1.0;
  if(CosAngle > 1.0) CosAngle=1.0;
  return acos(CosAngle);
}
