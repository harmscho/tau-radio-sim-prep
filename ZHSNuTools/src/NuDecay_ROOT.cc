/* This is a simple library to read Pablo's Nu decay root files in order to create jet information for zhaires runs. It of course uses ROOT, while the general ZHSNutools does not. This is why it's implemented separately

Possibly in the future deacy info could be stored in "generic" binary files, so no ROOT will be needed

Washington Carvalho April 2015*/



#include "NuDecay_ROOT.h"


//CONSTRUCTORS   ///////////////////////////////////////////

//default constructor
NuDecay_ROOT::NuDecay_ROOT():InputIsSet(false),DecayNIsSet(false),pArrayIsSet(false),Verbose(true)
{
    Hello();
}

//Constructor that sets input ROOT file
NuDecay_ROOT::NuDecay_ROOT(string filename):InputIsSet(false),DecayNIsSet(false),pArrayIsSet(false),Verbose(true)
{
    Hello();

    SetInputFile(filename);
}

//Constructor that sets input file and decay number
NuDecay_ROOT::NuDecay_ROOT(string filename, int decayn):InputIsSet(false),DecayNIsSet(false),pArrayIsSet(false),Verbose(true)
{
    Hello();

    SetInputFile(filename);
    
    SetDecayN(decayn);
    
    ReadDecay();
}
///////////////////////////////////////////////////////////


void NuDecay_ROOT::SetInputFile(string filename)
	{
	    //test file extension
	    if(filename.substr(filename.find_last_of(".") + 1) != "root")
	    {
		cout<<"(EE) NuDecay_ROOT::SetFilename error: Input file "<<filename<<" extension is not .root! Exiting..."<<endl;
		exit(-1);
	    }
	    //Set input file
	    InputFile=filename;
	    InputIsSet=true;

	    //Reset array
	    pArray.clear();
	    pArrayIsSet=false;

	    return;
	}


void NuDecay_ROOT::SetDecayN(int decayn)
{
    if(decayn<1)
    {
	cout<<"(EE) NuDecay_ROOT::SetDecayN error: decayn must be positive! Exiting..."<<endl;
	exit(-1);
    }  
  DecayN=decayn;
  DecayNIsSet=true;

  return;
}

void NuDecay_ROOT::ReadDecay()
{
    if(!InputIsSet) 
    {
	cout<<"(EE) NuDecay_ROOT::ReadDecay error: Input file not set! Exiting..."<<endl;
	exit(-1);
    }
    
    if(!DecayNIsSet) 
    {
	cout<<"(EE) NuDecay_ROOT::ReadDecay error: DecayN not set! Exiting..."<<endl;
	exit(-1);
    }
    
    
    //Open Root file
    cout<<"Opening root file: "<<InputFile<<"..."<<endl;
    TFile *fin=TFile::Open(InputFile.c_str(),"READ");
    
    

    //read root tree
    TTree *mainTree=(TTree*)fin->Get("mainTree;1");
    
    int NdecaysInFile=mainTree->GetEntries(); //Number of decays in the tree

    if(Verbose) cout<<"Input file contains "<<NdecaysInFile<<" decays."<<endl;
    
    if(DecayN>NdecaysInFile)
    {
	cout<<"(EE) NuDecay_ROOT::ReadDecay error: DecayN larger than number of decays in file! Exiting..."<<endl;
	exit(-1);
    }


    mainTree->SetBranchAddress("nPartTau",&nPart);
    mainTree->GetEntry(DecayN);

    

    if(Verbose) cout<<"Reading decay #"<<DecayN<<" with "<<nPart<<" secondary particles..."<<endl;


    int pcode[nPart];
    float ppx[nPart],ppy[nPart],ppz[nPart],pE[nPart];
    mainTree->SetBranchAddress("pCodeTau",&pcode);
    mainTree->SetBranchAddress("pxTau",&ppx);
    mainTree->SetBranchAddress("pyTau",&ppy);
    mainTree->SetBranchAddress("pzTau",&ppz);
    mainTree->SetBranchAddress("pETau",&pE);
    mainTree->SetBranchAddress("tauTotalE",&TotalE);
    mainTree->SetBranchAddress("showerTotalE",&ShowerTotalE);
    mainTree->SetBranchAddress("tauDecayXc", &DecayXc);
    mainTree->SetBranchAddress("tauDecayH10", &DecayH10);
    mainTree->SetBranchAddress("tauDecayH5", &DecayH5);
    mainTree->SetBranchAddress("theta",&Theta);
    mainTree->SetBranchAddress("phi",&Phi);
    
    mainTree->GetEntry(DecayN);

    
    Particles p;
    for(int i=0;i<nPart;i++)
    {
	p.Code=pcode[i];
	p.px=ppx[i];
	p.py=ppy[i];
	p.pz=ppz[i];
	p.E=pE[i];	
	pArray.push_back(p);
	
	if(Verbose) cout<<"p.Code="<<p.Code<<" p=("<<p.px<<","<<p.py<<","<<p.pz<<") p.E="<<p.E<<" size of array is now "<<pArray.size()<<endl;

    }

    cout<<"Read decay #"<<DecayN<<" with "<<nPart<<" particles."<<endl;
    
    fin->Close();
    //mainTree->Delete();
    
    pArrayIsSet=true;
    
    
    return;
}

void NuDecay_ROOT::ReadDecay(string filename, int decayn)
{
    SetInputFile(filename);

    SetDecayN(decayn);
   
    ReadDecay();
    return;
}

void NuDecay_ROOT::Hello()
    {
	cout<<"********************************"<<endl;
	cout<<"*    NuDecay_ROOT v"<<VERSIONDR<<"         *"<<endl;
	cout<<"********************************"<<endl;
    }

// int NuDecay_ROOT::GetNDecays(string filename)
// {
//     //open file (untested!)
//     TFile *fin=TFile::Open(inputFile.c_str(),"READ");

//     //read root tree
//     TTree *mainTree=(TTree*)fin->Get("mainTree;1");
    
//     return mainTree->GetEntries(); //Number of decays in the tree
// }    
