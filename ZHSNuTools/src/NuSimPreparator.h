/* This is a simple library to read Pablo's Nu decay root files in order to create jet information for zhaires runs. It of course uses ROOT, while the general NuSimPreparator does not. This is why it's implemented separately

Possibly in the future deacy info could be stored in "generic" binary files, so no ROOT will be needed

Washington Carvalho April 2015*/
#ifndef NUSIMPREPARATOR_H
#define NUSIMPREPARATOR_H

#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include <math.h>
#include "Particles.h"

#include "NuDecay_ROOT.h"
#include "AntennaArray.h"
#include "Vector3D.h"
#include "AtmUtils.h"

#define VERSIONSP "0.1"
#define MINE "1e15"
#define MAXE "1e22"
#define MAXENERGYFACTOR "3"
#define MINENERGYFACTOR "0.33"
#define MINTHINNING "1e-7"
#define MAXTHINNING "1e-4"


using namespace std;

class NuSimPreparator {

public:
    //CONSTRUCTORS   ///////////////////////////////////////////

    //default constructor
    NuSimPreparator();

    //Constructor that sets the "real" shower geometry always in DEGREES
    //decayheight (height above ground where decay occurs) in m
    //groundaltitude in m
    NuSimPreparator(double thetadeg, double phideg, double decayheight, double groundaltitude);

    //Constructor that sets shower geometry (DOWNGOING and in DEGRRES and M)
    //and the decay input file and decay number for retrieving secondary
    // particles from the decay file and desired energy in eV
    NuSimPreparator(double desiredtauenergy, double thetadeg, double phideg, double decayheight, double groundaltitude, string inputfile, int decayn);

    //Same as above, but set InvertPhi to Off (antennas above ground)
    NuSimPreparator(double desiredtauenergy, double thetadeg, double phideg, double decayheight, double groundaltitude, string inputfile, int decayn, bool invertphi);

    /////////////////////////////////////////////////////////// 
    //VARIABLES //////////////////////////////////////////////
    //////////////////////////////////////////////////////////


    //Particles struct with decay secondary particle data (defined in NuDecay.h)
    Particles pcles; 
    
    //Desired energy of the primaty decaying tau
    //IMPORTANT NOTE: if the TauEnergy in the decay input file differs from 
    //the input DesiredTauEnergy, the energy E of the decay products will be 
    //scaled to this new desired energy using:
    //   E' = E * DesiredTauEnergy/TauEnergy
    double DesiredTauEnergy; //Desired energy for the shower (eV)
    

    //shower geometry //////////////////////////////////////////// 
    //////////////////////////////////////////////////////////////
    //NOTE: the real shower is upgoing, but Aires cannot handle that, so we
    //create a "fake" downgoing shower but the injected "jet" special 
    //primary has upwards momentum. 
    //Also, ZHAireS has a time window calculation that presupposes an upgoing 
    //shower. So in order to simulate we use a fake shower axis that comes 
    //from the opposite direction (azimuthal angle phi+180 deg).
    //In Aires coordinate system, the azimuthal angle is w.r.t. the x-axis
    //(magnetic North) increasing towards the West and represents the direction
    //the shower is coming from.

    //Theta and Phi: Real shower axis direction (still downgoing, so 
    //Theta<=90 deg ). Phi represents the direction the shower is coming from
    double Theta,Phi;   //Zenithal and azimuthal angles of the real shower (rad)
    double ThetaDeg,PhiDeg; //same as above, in degrees
    
    double AiresTheta;      //Zenithal angle for the fake Aires axis (rad)
    double AiresThetaDeg;   //same as above, in degrees

    double AiresPhi;        //Azimuthal angle for the fake Aires axis (rad)
    double AiresPhiDeg;     //same as above, in degrees 
    
    double DecayHeight;     //height above ground where decay occurs
    double GroundAltitude;  //Ground altitude in m

    double DecayDistance;   //Distance from core to injection point in m
    double DecayGroundDistance;   //Distance from core to injection point in m
    
    


    //Decay information ////////////////////////////////////////////// 
    /////////////////////////////////////////////////////////////////

    //input to NuDecay
    string DecayInputFile;  //name of file to read decay from
    int DecayN;             //number of decay from decay file to use
    //output of NuDecay
    double TauEnergy;       //Energy of Tau (eV) 
    double ShowerEnergy;    //Visible energy of Tau induced shower (eV)
    
    vector<Particles> pArray;//Array of Particle structs with products of decay
    
    //DecayGometry 
    double Xinj,Yinj,Zinj;   //Injection point coordinates in the AIRES system
    
    //The decay time is set so that a plane shower front (that is actually 
    //upgoing) would cross the Core at t=0 (i.e. even before the injection 
    //time, so this front actually does not exist in t=0). That is done this 
    //way because zhaires expects t=0 when the shower front reaches the ground
    // for the time window calculations

    double DecayTime;      //Decay time in ns

    //Vectors will be used in the Core coordinate system, i.e. the Aires system,
    //but with the origin not at sea level but at the core (ground) level.
    //(see AntennaArray.h for more info on the Core CS)
    
    

    //Decay position in Core CS
    Vector3D vDecay;
    Vector3D vAxis; //upgoing axis direction in CoreCS  (normalized vDecay)
    
    //Variables for the Estimate of Xmax 
    //(for "impact point" calculations for array optimization
    double DXmax;      //distance (along shower axis) from decay to Xmax (m)
    Vector3D vXmax;   //Xmax position (in Core CS)
    


    //Variables for optimized array creation 
    double R0,kr; //parameters of the n model (same as in ZHAireS)
    double CherAngle,CherAngleDeg;  //Cherenkov angle at Xmax (rad and deg)
    //Psi is the angle between the shower axis and the propagation direction
    //from Xmax. It is the opening of the "emission" cone centered at Xmax.
    //For the Cherenkov cone Psi is the Cherenkov angle
    double MinPsi,MaxPsi;  //Minimum and maximum Psi angles 
    //Maximum angular diference from Cherenkov angle to minPsi (inside cone)
    double MaxAngleIn,MaxAngleInDeg; //default 0.5 deg
    //Maximum angular diference from Cherenkov angle to maxPsi (outside cone)
    double MaxAngleOut,MaxAngleOutDeg; //default 0.8 deg
    Vector3D vImpactPoint;

    bool FilterArray; //if true array will be filtered according to Max/MinPsi
    AntennaArray Array;
    vector<ArrayAntenna> Antennas;

    //Variables with Aires parameters ///////////////////////////////
    /////////////////////////////////////////////////////////////////
    string TaskName;          //Aires Task Name
    double ThinningEnergy;    //Relative Thinning Energy
    double RandomSeed;        //Aires seed
    double TimeDomainBin;     //Timed domain bin size in ns 
    double GeomagneticFielduT; //Geomagnetic field amplitude in uT 
                               //(default 56.0 uT)
    double GeomagneticInclinationDeg;  //Geomagnetic field inclination in Deg
                                       //(default 63.5 deg)
	
    //variables for the queue scripts //////////////////////////////
    ////////////////////////////////////////////////////////////////
    string User;  //default is washington.carvalho
    string Queue; //default is auger.q
    int Priority; //default is 0

    
    ////////////////////////////////////////////////////////
    //Methods /////////////////////////////////////////////
    ////////////////////////////////////////////////////////



    //"Action" Methods //////////////////////////////////////
    ////////////////////////////////////////////////////////

    //read decay from file (uses external reader struct/class
    void ReadDecay();
    void ReadDecay(string filename,int decayn);
    
    void CreateSpecialParticleFile();
    void CreateAiresInpFile();
    
    //creates optimized array
    //d is distnce between antennas (see AntennaArray.h for more details) 
    void CreateAntennaArray(double d, double length, double width); //use m

    //creates perpendicular antenna array
    //dxmax is distance between aarray and Xmax 
    void CreatePerpendicularAntennaArray(double d, double length, double width, double dxmax); //use m
    void CreateAnitaPerpendicularAntennaArray(double d, double length, double width, double anitaaltitude); //use m

    //Messages and dump methods /////////////////////////////
    ////////////////////////////////////////////////////////

    void DumpShowerGeometry(); //tests and prints shower geometry
    void DumpDecayInput();    //tests and prints DecayInputFile and DacayN
    void DumpParticleArray();
    void Hello();
        
    //Set Methods //////////////////////////////////////
    ////////////////////////////////////////////////////

    // Shower geometry
    
    void SetGroundAltitude(double groundaltitude);
    void SetDecayHeight(double decayheight);
    void SetShowerGeometry(double airesthetadeg, double airesphideg, double decayheight, double groundaltitude);
    void SetUsePlaneEarthOn() {UsePlaneEarth=true;};
    void SetUsePlaneEarthOff() {UsePlaneEarth=false;};
    void SetDXmax(double dxmax) {DXmax=dxmax;};
    void SetDXmaxgcm2(double T); //sets Dxmax for a thickness of T g/cm2

    //decay Input
    void SetDesiredTauEnergy(double desiredtauenergy);
    void SetDecayN(int decayn);
    void SetDecayInputFile(string inputfile);
   
    // Aires parameters
    void SetThinningEnergy(double thinnigenergy); //relative
    void SetTaskName(string taskname);
    void SetRandomSeed(double seed){RandomSeed=seed;};
    void SetTimeDomainBin(double nsbin){TimeDomainBin=nsbin;};
    void SetGeomagneticFielduT(double field){GeomagneticFielduT=field;};
    void SetGeomagneticInclinationDeg(double angle){GeomagneticInclinationDeg=angle;};
	 
    //array parameters
    void SetMaxAngleInDeg(double maxangle){MaxAngleInDeg=maxangle;};
    void SetMaxAngleOutDeg(double maxangle){MaxAngleOutDeg=maxangle;};
    void SetFilterArrayOn(){FilterArray=true;};
    void SetFilterArrayOff(){FilterArray=false;};
   
    //Set verbose methods
    void SetVerboseOn() {Verbose=true;};
    void SetVerboseOff() {Verbose=false;};
    //Set InvertPhi=false to use antennas above ground level
    //A Zhaires version for upgoing showers (inverted time windows dtna) must
    //be used instead of regular zhaires!!!
    //Necessary for ANITA like simulations!!
    void SetInvertPhiOff() {InvertPhi=false;};

    //Set methods for queue script variables
    void SetUser(string user){User=user;};
    void SetPriority(int priority){Priority=priority;};
    void SetUseRoot(bool useroot){UseRoot=useroot;};

    

    
    //Check methods //////////////////////////////////////
    ////////////////////////////////////////////////////

    bool CheckAngles(double thetadeg, double phideg);
    bool CheckDecayInput();
    bool CheckParticleArray();


    

    

    

    //Get methods //////////////////////////////////////
    ////////////////////////////////////////////////////

    double GetThetaDeg(){return ThetaDeg;};
    double GetPhiDeg(){return PhiDeg;};
    double GetTheta(){return Theta;};
    double GetPhi(){return Phi;};
    double GetAiresThetaDeg(){return AiresThetaDeg;};
    double GetAiresPhiDeg(){return AiresPhiDeg;};
    double GetAiresTheta(){return AiresTheta;};
    double GetAiresPhi(){return AiresPhi;};
    bool GetInvertPhi(){return InvertPhi;};

    ///////////////////////////////////////////////////
    ///////////////////////////////////////////////////
    ///////////////////////////////////////////////////


//private:

    double EnergyScaleFactor;       // scale factor: DesiredTauEnergy/TauEnergy

    //Special particle file //////////////////////
    string SpecialParticleFileName; //Aires special particle .f code filename
    string SpecialParticleExecutableFileName; //Name after compilation (no .f)
    //Aires .inp file //////////////////////
    string AiresInpFileName;
        
    //Helper/speedup variables
    double CosTet, CosPhi,SinTet, SinPhi;

    //variables for atmospheric "thickness" integration
    double stepm;      //step for the integration in m
    double precision;  //Maximum "error" for the integration
   
    string Version;

    //Flags /////////////////////////////////////////
    bool Verbose; //sets verbose mode (for now default is true)
    bool ShowerGeometryIsSet;
    bool DecayIsSet;
    bool UseRoot; //if true read decay from root file (default: true)
                  //if false read from  NuDecay (not yet implemented)

    bool UsePlaneEarth;  //Calculates Decay geometry with plane earth approxim.
    bool SpecialParticleFileWritten;
    bool AiresInpFileWritten;
    bool ArrayIsSet;
    //inverts phi: adds 180deg to shower phi direction. For use with regular 
    //             zhaires and antennas at ground level only
    // To use antennas above ground (e.g. for ANITA) this must be set to false
    // and the zhaires version used to simulate the shower has to have inverted
    // time window calculation (because shower is upgoing!)
    bool InvertPhi;  

    bool FileExists(string filename)
    {
	ifstream intest(filename.c_str());
	if( intest.good() ) return true;
	else return false;
    }



};

double pi=acos(-1);
double d2r=pi/180.;
double r2d=180./pi;
double pi_2=pi/2.;


#endif
