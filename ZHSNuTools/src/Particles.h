#ifndef PARTICLES_H
#define PARTICLES_H

struct Particles {
    int   Code;        //code of particle
    double px,py,pz;     //momentum of particle (check?)
    double E;           //Energy of particle
};

#endif
