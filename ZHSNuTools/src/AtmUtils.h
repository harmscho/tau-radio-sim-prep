#ifndef ATMUTILS_H
#define ATMUTILS_H

#include<math.h>
#include<iostream>
#include <cstdlib>

using namespace std;

double gcm2toh(double t);

double htogcm2(double h);

void  Atmosphere(float alt, float &density, float &temperature, float &preassure);

#endif
