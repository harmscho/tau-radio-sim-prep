/* This is a simple library to read Pablo's Nu decay root files in order to create jet information for zhaires runs. It of course uses ROOT, while the general NuSimPreparator does not. This is why it's implemented separately

Possibly in the future deacy info could be stored in "generic" binary files, so no ROOT will be needed

This is the struct/class that deals with creating and adjusting an array for the simulation.

Washington Carvalho April 2015*/

#ifndef ANTENNARRAY_H
#define ANTENNARRAY_H

#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include <math.h>

#include "Vector3D.h"

#define VERSIONAA "0.1"

using namespace std;

//Not name Antenna because that struct exists in ZHSEvent
struct ArrayAntenna{

ArrayAntenna():
Id(-1),
Psi(-1)
{};
    int Id; //Id>=1
    double x,y,z; //Antenna coordinates
    double Psi;  //rad
};

class AntennaArray {


    
    //By default the array is a "square" array with distance D between 
    //the antennas. The length and the width of the array are its dimensions
    //along the NS and EW directions, respectively.

    //Rotations and translations of the array will be implemented later

    //The array has its own coordinate system, whith the origin at its center, 
    //which contains an antenna. This system is called ArrayCS.
    //This array system, as the AIRES CS, has x towards the N, Y towards the W
    //and Z upwards. Initial Z coordinate is always 0 (ground)
    //So the antenna coordinates are in the range:
    //
    //  -LENGTH/2 < X < LENGTH/2,  -WIDTH/2 < Y < WIDTH/2  and  Z=0 (Z>0 later) 
    //
    //                           ARRAY CS
    //
    //                              ^ X (N: length) 
    //                              |
    //                              |        
    //                 Y(W: width)  |
    //                       <------|--------
    //                              |
    //                              |          Z=0
    //                              |
    //
    
    //Once the core coordinates are set (in the Array CS), the antenna
    //coordinates are then transformed to the shower core system, called 
    //Core CS, with its origin at the core on the ground. 

    //Once in the Core Cs, the array can be rotated and translated, e.g. to
    //position antennas not in the ground plane, but in the shower
    //perpendicular plane. But the antenna coordinates will always be given 
    //in terms of the Core CS, which is the input coordinate system for 
    //antennas in ZHAireS.
    //In this Core CS, the Z coordinate refers to the antenna height above the
    //ground level set in ZHAireS.


public:
    //CONSTRUCTORS   ///////////////////////////////////////////

    //default constructor
    AntennaArray();


    //Constructor that sets distance D (in m) between antennas,
    //the  Length (in m) and the Width (in m) of the array.
    AntennaArray(double d,double length, double width);

    //Constructor that sets array dimensions and core position (in m)
    //NOTE: core position given in the ArrayCS
    AntennaArray(double d,double length, double width, double corex, double corey, double corez);

    

    /////////////////////////////////////////////////////////// 
    //VARIABLES //////////////////////////////////////////////
    //////////////////////////////////////////////////////////
    
    double D;            //distnace between antennas
    double Length;       //Length of the array (dimension along the NS line)
    double Width;       //Length of the array (dimension along the NS line)

    //Core (Vector3D) described in the Array coordinate system (ArrayCS)
    Vector3D Core;

    //Xmax (Vector3D) described in the Core coordinate system (CoreCS)
    Vector3D Xmax;

    vector<ArrayAntenna> aArray;          //Antenna vector in Array CS
    vector<ArrayAntenna> aArrayCoreCS;    //Array in Core CS
    
    
    vector<ArrayAntenna> aArrayFiltered;  //Filtered array (Core CS)
    //Note:Filtered antennas have the same Id as the unfiltered array 


    
    


    //Check methods ////////////////////////////////////////////
    ////////////////////////////////////////////////////////
    bool CheckDimensions(double d, double length, double width);

    //Set methods ////////////////////////////////////////////
    ////////////////////////////////////////////////////////

    //Sets Array dimensions and calculates aArray (in Array CS)
    void SetDimensions(double d, double length, double width);


    //Sets coord. of core (in array CS) then also calculates aArrayCoreCS
    void SetCore(double xc, double yc, double zc); 
    
    //Set coords of Xmax in the Core system (in m)
    void SetXmax(double xmaxx,double xmaxy,double xmaxz);

    void SetVerboseOn(){Verbose=true;};
    void SetVerboseOff(){Verbose=false;};
    void SetArrayFileName(string filename){ArrayFileName=filename;};
    void SetArrayCoreCSFileName(string filename){ArrayCoreCSFileName=filename;};
    void SetArrayFilteredFileName(string filename){ArrayFilteredFileName=filename;};
    void SetShowerIsUpgoingInCoreCS(bool b){ShowerIsUpgoingInCoreCS=b;};

    //Set variables for filtering
    void SetMinZ(double minz){MinZ=minz;};
    void SetMaxZ(double maxz){MaxZ=maxz;};
    void SetMinPsi(double minpsi){MinPsi=minpsi;};
    void SetMaxPsi(double maxpsi){MaxPsi=maxpsi;};
    void SetMinPsiDeg(double minpsi){MinPsi=minpsi*acos(-1)/180.;};
    void SetMaxPsiDeg(double maxpsi){MaxPsi=maxpsi*acos(-1)/180.;};

    
    //Get methods ///////////////////////////
    /////////////////////////////////////////

    double GetMinZ(){return MinZ;};
    double GetMaxZ(){return MaxZ;};
    double GetMinPsi(){return MinPsi;};
    double GetMaxPsi(){return MaxPsi;};

    //"Action" methods
    /////////////////////////////////////////
    void CalculateArrayCoreCS();
    void CalculatePsi();

    //Rotates the array by phi around the z-axis and then theta around
    //the new y-axis. Used for the Core CS array only. 

    //(For angles in degrees)
    void RotateArrayCoreCSDeg(double thetadeg,double phideg);
    //(For angles in rad)
    void RotateArrayCoreCS(double theta,double phi);

    //Translates the array by the vector (x,y,z) or Vector3D vt=(x,y,z)
    void TranslateArrayCoreCS(double x, double y, double z);
    void TranslateArrayCoreCS(Vector3D vt);

    //Transforms array CoresCS to be perpendicular to the shower axis
    //with core at a distance DXmax from Xmax 
    //(note that the ShowerIsUpgoingInCoreCS matters!)
    //Please don't use after rotations or translations, as it 
    //assumes the "default" ground array for the calculations
    //of course ArrayCoreCS and Xmax must have been set
    void CreatePerpendicularArrayCoreCS(double dxmax);


    //Filter array (according to set MinZ, MaxZ, MinPsi and MaxPsi)
    void FilterArray();
    

    //Messages and dump methods /////////////////////////////
    ////////////////////////////////////////////////////////
    void Hello();

    void WriteArrayFile();
    void WriteArrayCoreCSFile();
    void WriteArrayFilteredFile();

  

private:
    int CurrentId;
    string ArrayFileName;        //ASCII file with data in aArray
    string ArrayCoreCSFileName;  //ASCII file with data in aArrayCoreCS
    string ArrayFilteredFileName;  //ASCII file with filtered array
    bool Verbose;
    
    //variables for filetring the array (no filtering in X and y!!)
    double MinZ,MaxZ,MinPsi,MaxPsi;
    
    
    //Flags
    bool ArrayDimensionsAreSet; //true after array D, length and width are set
    bool CoreIsSet;             //true after core is set
    bool XmaxIsSet;             //true after Xmax is set
    bool ArrayIsSet;            //true after aArray is calculated
    bool ArrayCoreCSIsSet;      //true after aArrayCoreCS is calculated
    bool ArrayFilteredIsSet;      //true after aArrayCoreCS is calculated
    bool ArrayCoreCSWasTransformed;//true if aArrayCoreCS was rotated/translated

    bool ShowerIsUpgoingInCoreCS;  //true if it is an upgoing shower 
    //Used for Psi calculation. Default: true
    //NOTE: If the array is ABOVE Xmax, shower should be taken to be downgoing, 
    //      although it goes up in the atmosphere. That is it is downgoing in 
    //      the CoreCS system.


    //Methods ///////////////////////
    /////////////////////////////////
    
    bool FileExists(string filename)
    {
	ifstream intest(filename.c_str());
	if( intest.good() ) return true;
	else return false;
    }

};

#endif
