#include "NuDecay_ROOT.h"

int main()
{
    //Pars
    string name="NuDecaysE1e17.root";
    int decayn=1687;

    
    //test default constructor;
    NuDecay_ROOT d1;


    //test constructor with filename only
    NuDecay_ROOT d2(name);
    d2.SetVerboseOn();
    //d2.SetVerboseOff();
    d2.ReadDecay(name,decayn);

    //test full constructor
    NuDecay_ROOT d3(name,decayn);


    cout<<"File "<<name<<" contains "<<GetNDecays(name)<<" decays!"<<endl;
    
}
