#include "AtmUtils.h"

double gcm2toh(double t)
{
  //transforms vertical thickness t [g/cm**2] in altitude h [km]
  //functions h1km and h2km are the inverse of Linsley parametrization
  // for US standard atmosphere
  double h;
  int par;
  if(t < 0.0) 
    {
      cerr<<"ERROR: atmospheric thickness is negative"<<endl;
      return 0;
    }
  if(t <= 0.00128293)
    {
	h=1.e4*(1.12829e-2-t);
	return h;
    }
  
  double a[4]={-1.86556e2 , -9.4919e1 , 6.1289e-1 , 0.0};
  double b[4]={1.2227e3 , 1.1449e3 , 1.3056e3 , 5.4018e2};
  double c[4]={9.9419 , 8.7815 , 6.3614 , 7.7217};
  
  if (t <= 3.0395) par=3;
  else if (t <= 271.6991) par=2;
  else if (t <= 631.1) par=1;
  else if (t <= 2004.7) par=0;
  else
    {
      cerr<<"ERROR: atmospheric thickness above 2004.647"<<endl;
      return 0;
    }
  
  h=c[par]*log(b[par]/(t-a[par]));
  return h;
}




double htogcm2(double h)
{
    //functions are the the Linsley parametrization
    // for US standard atmosphere
  
  double t;
  int par;
  
  if(h < -5.801)
    {
      cout<<h;
      cout<<"ERROR: altitude lower than -5.8 km"<<endl;
      exit(-1);
    }
  
  if(h > 112.8)
    {
      t=0.0;
      return t;
    }
  
  if(h >= 100.0)
    {
	t=1.12829e-2-h/1.e4;
	return t;
    }

  double a[4]={-1.86556e2 , -9.4919e1 , 6.1289e-1 , 0.0};
  double b[4]={1.2227e3 , 1.1449e3 , 1.3056e3 , 5.4018e2};
  double c[4]={9.9419 , 8.7815 , 6.3614 , 7.7217};
  
  if (h >= 40.0) par=3;
  else if (h >= 10.0) par=2;
  else if (h >= 4.0) par=1;
  else par=0;
  
  t=a[par]+b[par]*exp(-h/c[par]);
  return t;
}


void  Atmosphere(float alt, float &density, float &temperature, float &preassure)
{
  // traduzido de  http://www.pdas.com/programs/atmos.f90
  /*
    !   -------------------------------------------------------------------------
    ! PURPOSE - Compute the properties of the 1976 standard atmosphere to 86 km.
    ! AUTHOR - Ralph Carmichael, Public Domain Aeronautical Software
    ! NOTE - If alt > 86, the values returned will not be correct, but they will
    !   not be too far removed from the correct values for density.
    !   The reference document does not use the terms pressure and temperature
    !   above 86 km.
  */
  
  /*
    !============================================================================
    !     A R G U M E N T S                                                     |
    !============================================================================
    REAL,INTENT(IN)::  alt        ! geometric altitude, km.
    REAL,INTENT(OUT):: sigma      ! density/sea-level standard density
    REAL,INTENT(OUT):: delta      ! pressure/sea-level standard pressure
    REAL,INTENT(OUT):: theta      ! temperature/sea-level standard temperature    
  */
  /*
    !============================================================================
    !     L O C A L   C O N S T A N T S                                         |
    !============================================================================
  */
  const float REARTH = 6369.0;                 // radius of the Earth (km)
  const float GMR = 34.163195;                 // hydrostatic constant
  const int NTAB = 8;                // number of entries in the defining tables

  /*
    !============================================================================
    !     L O C A L   A R R A Y S   ( 1 9 7 6   S T D.  A T M O S P H E R E )   |
    !============================================================================
  */
  const float htab[NTAB]= {0.0, 11.0, 20.0, 32.0, 47.0, 51.0, 71.0, 84.852};
  
  const float ttab[NTAB]= {288.15, 216.65, 216.65, 228.65, 270.65, 270.65, 214.65, 186.946};
  const float ptab[NTAB]= {1.0, 2.233611E-1, 5.403295E-2, 8.5666784E-3, 1.0945601E-3, 6.6063531E-4, 3.9046834E-5, 3.68501E-6};
  const float gtab[NTAB]= {-6.5, 0.0, 1.0, 2.8, 0.0, -2.8, -2.0, 0.0};
  
  //--------------------------------------------------------------------------
  
  float h=alt*REARTH/(alt+REARTH);      // convert geometric to geopotential altitude
  
  int i=0;
  
  if (h > 84.85) i = NTAB - 1;
  else while (h > htab[i+1]) i++;
  
  float tgrad=gtab[i];                 // temperature gradient of this layer             
  float tbase=ttab[i];                 // base temp of this layer
  float deltah=h-htab[i];              // height above base of this layer
  float tlocal=tbase+tgrad*deltah;     // local temperature
  float theta=tlocal/ttab[0];                // temperature ratio
  
  float delta;
  if (tgrad == 0.0)                     // pressure ratio
    delta=ptab[i]*exp(-GMR*deltah/tbase);
  else
    delta=ptab[i]*pow((tbase/tlocal),(GMR/tgrad));
  
  float sigma=delta/theta;                    // density ratio

  
  temperature = ttab[0]*theta;  // temperature in Kelvin
  preassure = 760*delta;        // preassure in mm Hg
  density = 1.20447e-3*sigma;         // density in g/cm^3
  density*=1e3; //density in kg/m3
  
  return;
}
