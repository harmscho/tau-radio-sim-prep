#include "NuDecay_ROOT.h"
#include "NuSimPreparator.h"
#include "AntennaArray.h"
#include "TMath.h"

#include <fstream>
#include <stdlib.h>
using namespace std;


double GetDistance( double zenith, double altitude, double ground){
  
  double R_E = 6356752.3; //polar radius of Earth
  
  
  double cosZen = cos(zenith * TMath::DegToRad());
  
  double a = 1;
  double b = 2 * (R_E + ground) * cosZen;
  double c = ((R_E + ground) * (R_E + ground)) - ((R_E + altitude) * (R_E + altitude));
  
  double dplus = (-b + sqrt(b*b - 4 * a * c))/ (2 * a);
  
  //  double dmin = -R_E * cosZen - sqrt((R_E * R_E * cosZen * cosZen) + (2 * R_E * altitude) + (altitude * altitude));
  //  double dplus = -R_E * cosZen + sqrt((R_E * R_E * cosZen * cosZen) + (2 * R_E * altitude) + (altitude * altitude));
  
  //  cout << zenith <<  "d+: " << dplus/1000 <<  "\t d-: " << dmin/1000  << endl;
  return dplus;
}

void GenerateAntennas(double zenith /*deg*/ , double altitude /*m. a.s.l.*/, double dec_altitude /*m. a.s.l.*/, double ground,  ofstream& ofs, int nAnt = 20, double deltaPsi = 0.2, double azimuth=0 /*deg*/){
  azimuth *= TMath::DegToRad();
  
  //distance payload to emergence point: (0,0,0) - ZHAireS
  double distance = GetDistance(zenith,altitude, ground);
  cout << "distance to payload" << distance << endl;
  double x,y,z;
  //altitude of the antennas at fixed z in ZHS
  z = cos(zenith *  TMath::DegToRad()) * distance;
  //horizontal distances in ZHS
  double r = sin(zenith *  TMath::DegToRad()) * distance;
  //antenna angular spacing
  deltaPsi *= TMath::DegToRad();
  //distance to decay point
  cout << "dec_altitude " << dec_altitude << "\t ground " << ground << " zenith" << zenith << endl;
  double dec_dist = GetDistance(zenith,dec_altitude+ground, ground);
  //x,y,z of decay point (ZHAireS)
  double dec_x = sin(zenith * TMath::DegToRad()) * dec_dist * cos(azimuth);
  double dec_y = sin(zenith * TMath::DegToRad()) * dec_dist * sin(azimuth);
  double dec_z = cos(zenith * TMath::DegToRad()) * dec_dist;
  
  //for check
  double ax = sin(zenith * TMath::DegToRad()) * cos(azimuth);
  double ay = sin(zenith * TMath::DegToRad()) * sin(azimuth);
  double az = cos(zenith * TMath::DegToRad());
  
  cout << "distance to decay " << dec_dist << endl;
  //distance from decay point to antennas
  distance -= dec_dist;
  
  for (int iAnt = 0; iAnt < nAnt; iAnt++) {
    double x_ = r;
    double y_ = distance * tan(iAnt * deltaPsi);
    //rotation for azimuth
    x = cos(azimuth) * x_ - sin(azimuth) * y_;
    y = sin(azimuth) * x_ + cos(azimuth) * y_;
    cout << "AddAntenna " << x << "\t " << y << "\t" << z << endl;
    ofs << "AddAntenna " << x << "\t " << y << "\t" << z << endl;
    double len = sqrt(((x -dec_x)* (x -dec_x)) + ((y -dec_y) *(y -dec_y)) + ((z -dec_z) * (z -dec_z)));
    cout << "CHECK: " << endl;
    double offaxis = acos((ax * (x-dec_x) + ay * (y-dec_y) + az * (z-dec_z))/len);
    cout << offaxis * TMath::RadToDeg() << endl;
  }
  
}



void AddParticleOutput(string inFile){
	ifstream ifs(inFile.c_str());
	ofstream ofs("tmp.inp");
	string line;
	while (getline(ifs,line)){
		if (line.find("ObservingLevels") != string::npos) {
			/*
			 4000 m = 631.306 g/cm2 (630 - 500)/5 = 26
			 2000 m = 813.551 g/cm2 level (815 - 500)/5 = 63
			*/
		 	ofs << "ObservingLevels 100 500 g/cm2 1000 g/cm2" << endl;
		} else if (line.find("lgtpcles") != string::npos) {
			ofs << "SaveInFile lgtpcles All" << endl; 
			ofs << "RecordObsLevels    None" << endl;
			ofs << "RecordObsLevels    26" << endl;
			ofs << "PrintTable 1291" << endl;
			ofs << "PrintTable 1707" << endl;
			ofs << "PrintTable 2207 Opt d" << endl;	
			ofs << "PrintTable 3001 Opt M" << endl;
		} else {
			ofs << line << endl;
		}
	
	
	}
	
	string sCmd = "mv tmp.inp " + inFile;
	system(sCmd.c_str());
	
}


int main(int argc, char** argv)
{
  cout << "Generating Aires Input " << endl;
    //Pars
    string name="/home/hin/harmscho/radio/TauNeutrinoRadio/ZHSNuTools/files/NuDecaysE6e16.root";
    int decayn=10;//what is this
    double thetadeg=45.0;
    double phideg=0.;
    double groundaltitude=0.;
    double decayheight=1.0;
    double desiredtauenergy=6e15;
    double thinningenergy=1.e-4;
    double alt_ant = 35000;
    int n_ant = 20;
    double delta_psi = 0.1;
    //GeomagneticField 56 uT 63.5 deg 0.0 deg
  double magneticfield_microT = 56;
  double magneticinclination_deg = 63.5;
    if (argc == 1) {
      cout << "Running with default values! " << endl;
     
    } else if (argc == 12){
      name = argv[1];
      decayn = atoi(argv[2]);
      thetadeg = atof(argv[3]);
      phideg = atof(argv[4]);
      groundaltitude = atof(argv[5]);
      decayheight = atof(argv[6]);
      desiredtauenergy = atof(argv[7]);
      thinningenergy = atof(argv[8]);
      alt_ant = atof(argv[9]);
      n_ant = atoi(argv[10]);
      delta_psi = atof(argv[11]);
    } else if (argc == 14){
      name = argv[1];
      decayn = atoi(argv[2]);
      thetadeg = atof(argv[3]);
      phideg = atof(argv[4]);
      groundaltitude = atof(argv[5]);
      decayheight = atof(argv[6]);
      desiredtauenergy = atof(argv[7]);
      thinningenergy = atof(argv[8]);
      alt_ant = atof(argv[9]);
      n_ant = atoi(argv[10]);
      delta_psi = atof(argv[11]);
      magneticfield_microT = atof(argv[12]);
      magneticinclination_deg = atof(argv[13]);

    } else {
      cout << "usage: generateAiresInput <decay-root-file> <n-decay> <theta[deg]> <phi[deg]> <decayHeight m (1.0)> <tau-energy> <rel. thin-energy> <altitude-antennas [m]> <n-antennas> <separation-ant [deg]> <|Bgeo| muT(optional)> < Binclination deg (optional)>" << endl;
       return 0;
}
    //parameters needed to setup the array
    double dxmaxgcm2=500.0;
    //cherenkov angle - MaxAngleInDeg
    double MaxAngleInDeg=0.5;
    //cherenkov angle + MaxAngleOutDeg
    double MaxAngleOutDeg=0.5;
    //array
    double d=30.;
    double length=1000.; 
    double width=1000.;
    double dxmax=30628; 
    bool invertphi=false;
    

	
    cout<<endl<<"TEST 3:"<<endl;

    NuSimPreparator event2(desiredtauenergy,thetadeg,phideg,decayheight,groundaltitude,name,decayn,invertphi);
    
    cout<<"InvertPhi="<<event2.GetInvertPhi()<<endl;
    
    

    event2.ReadDecay();
    event2.SetThinningEnergy(thinningenergy);
    event2.SetGeomagneticFielduT(magneticfield_microT);
    event2.SetGeomagneticInclinationDeg(magneticinclination_deg);
    event2.SetVerboseOn();
//    event2.SetDXmaxgcm2(dxmaxgcm2);
//    event2.SetMaxAngleInDeg(MaxAngleInDeg);
 //   event2.SetMaxAngleOutDeg(MaxAngleOutDeg);

  
        
    // event2.CreatePerpendicularAntennaArray(d, length, width, dxmax);

    //event2.Array.WriteArrayCoreCSFile();
    cout << "Creating Special Particle File " << endl;
    event2.CreateSpecialParticleFile();
    event2.CreateAiresInpFile();
    cout << event2.AiresInpFileName << endl;
    std::ofstream ofs;
    ofs.open (event2.AiresInpFileName.c_str(), std::ofstream::out | std::ofstream::app);
    ofs << "RefractionIndex 1.0003274" << endl;
    GenerateAntennas(thetadeg,alt_ant,decayheight,groundaltitude,ofs,n_ant,delta_psi,phideg);
    ofs.close();
    
    AddParticleOutput(event2.AiresInpFileName.c_str());
    
    //event2.DumpParticleArray();
    //AntennaArray array;
    //cout<<"InvertPhi="<<event2.GetInvertPhi()<<endl;
    
	
}
